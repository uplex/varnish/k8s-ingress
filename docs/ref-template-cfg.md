# TemplateConfig Custom Resource reference

This is the authoritative reference for the ``TemplateConfig``
[Custom Resource](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/),
which is provided as a development tool for rapid prototyping and
testing of the templates used by the controller to generate VCL. The
``TemplateConfig`` resource is only read by the controller when it is
invoked with the ``-devmode`` [command-line flag](ref-cli-options.md).

The fields of the ``TemplateConfig`` ``spec`` object stand for the
templates defined by the controller code in
[``pkg/varnish/vcl``](/pkg/varnish/vcl), and their values are strings.
When the controller is in dev mode and a ``TemplateConfig`` is
defined, each string that it specifies is parsed as the source of a Go
[``text/template``](https://golang.org/pkg/text/template/), using the
[function map](https://golang.org/pkg/text/template/#FuncMap) defined
in the code for that template (there is no facility for changing the
function map, other than changing the code). If the parse succeeds,
then the template for VCL generation is replaced by the new template.
If the parse fails, then the controller log reports the error, an
event of type ``Warning`` with reason ``SyncFailure`` is generated
with the error message, and the template is unchanged.

Once a template is changed, it is used by the controller for all VCL
generation, for all viking Services that the controller administers.
It is not possible to limit use of the new template to specific
services or namespaces, except to the extent that the controller is so
limited. If the same field is specified for more than one
``TemplateConfig`` resource, then the controller uses the most recent
definition that it could successfully parse.

When a ``TemplateConfig`` resource is deleted, then for each field in
``spec`` that it specifies, the corresponding template is reverted to
the value defined in the controller code.

**Note**: dev mode and ``TemplateConfig`` are intended for development
use, to easily test changes in VCL generation that may make their way
into the standard controller code. They are not meant for production
use. Misbehaviors that result from broken templates in
``TemplateConfig`` are not bugs. However, changes that you have
tested with ``TemplateConfig`` may form the basis of merge requests,
although for a merge request you can just as well change the template
source in [``pkg/varnish/vcl``](/pkg/varnish/vcl).

Examples for the use of ``TemplateConfig`` resources can be found in
the [``test/e2e/tmplcfg/``](/test/e2e/tmplcfg) folder.

## Using TemplateConfig

The Custom Resource is defined in the
[helm chart for the viking controller](/charts/viking-controller/). If
you are using another deployment method, it may be created with
[``templatecfg-crd.yaml``](/deploy/templatecfg-crd.yaml) in the
[``deploy/``](/deploy) folder:

```
$ kubectl apply -f deploy/templatecfg-crd.yaml
```

``TemplateConfig`` is only read by the controller when it is invoked
with the ``-devmode`` command-line flag. With helm this is achieved
with the ``extraArgs`` field of ``values.yaml``:

```
# values.yaml for the controller
vikingController:

  # ...
  
  extraArgs:
    - -devmode
```

In a ``Deployment`` manifest, it is set in the ``spec.args`` field
of the Pod template:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: viking-controller
spec:
  # ---
  template:
    spec:
      # ...
      args:
      - -devmode
```

## API Group, version and resource names

The API group in use for this project is
``ingress.varnish-cache.org``, currently at version ``v1alpha1``. So a
manifest specifying a ``TemplateConfig`` resource MUST begin with:
```
apiVersion: "ingress.varnish-cache.org/v1alpha1"
kind: TemplateConfig
```

Existing ``TemplateConfig`` resources can be referred to in ``kubectl``
commands as ``templateconfig``, ``templateconfigs`` or with the short
names ``tmplcfg`` or ``tmplcfgs``:

```
$ kubectl get templateconfigs -n my-namespace
$ kubectl describe tmplcfg my-templates
```

## ``spec``

The ``spec`` section of a ``TemplateConfig`` is required, and may
contain any of the fields described in the following. (It may contain
none of them, in which case no template is overridden.) The value of
each field MUST be a non-empty string. Since templates invariably
encompass many lines, it is convenient to use the YAML ``|`` notation
(vertical bar) to specify a multi-line string that continues until the
indentation moves back:

```
spec:
  ingress: |
    vcl 4.1;
    #...
```

For fields not specified in a ``TemplateConfig``, the corresponding
template is unchanged -- the controller continues to use the template
for VCL generation as currently defined.

### Avoid templating in the wrong tool

Because the VCL template is a go template, when using ``helm`` or
other templating tools, care needs to be taken to avoid templating in
that tool. [With helm, there are several options to achieve
this](https://github.com/helm/helm/issues/2798), but then simplest
might be to surround the VCL with ``{{` ... `}}``:

```
spec:
  ingress: |
    {{`
    vcl 4.1;
    #...
    `}}
```

### ``spec.ingress``

``spec.ingress`` specifies a template for genaration of VCL to
implement Ingress routing rules and backend configuration. This
includes VCL to implement configuration expressed in
[``BackendConfig``](ref-backend-cfg.md) resources.

VCL generated from this template appears first in the resulting VCL
source. So the ``vcl`` version line MUST appear first, otherwise VCL
loads will fail.

```
spec:
  # Template for Ingress routing rules and backend configuration.
  ingress: |
    vcl 4.1;

    import std;
    # ...
```

### ``spec.acl``

``spec.acl`` specifies a template for generation of VCL to implement
the [ACL](ref-varnish-cfg.md#specacl) feature of ``VarnishConfig``.

### ``spec.auth``

``spec.auth`` specifies a template for generation of VCL to implement
the [Basic/Proxy Authentication](ref-varnish-cfg.md#specauth) features
of ``VarnishConfig``.

### ``spec.reqDisp``

``spec.reqDisp`` specifies a template for generation of VCL to
implement the disposition of client requests, or
[req-disposition](ref-req-disposition.md) feature of
``VarnishConfig``.

### ``spec.rewrites``

``spec.rewrites`` specifies a template for generation of VCL to
implement the [rewrite](ref-varnish-cfg.md#specrewrites) feature of
``VarnishConfig``.

### ``spec.shard``

``spec.shard`` specifies a template for generation of VCL to implement
the [self-sharding](ref-varnish-cfg.md#specself-sharding) feature of
``VarnishConfig`` (see also the
[self-sharding doc](self-sharding.md)).
