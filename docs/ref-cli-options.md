# Controller command-line options

The [controller executable](/docs/dev.md) ``k8s-ingress`` can
be started with command-line options, and these can be specified
in the ``extraArgs`` field of the
[helm chart for the viking controller](/charts/viking-controller/):

```
# values.yaml for the controller
vikingController:

  # ...

  extraArgs:
    - -log-level=DEBUG
    # ...
```
If you are using another deployment method, set the CLI arguments in the
``spec.args`` field of the Pod template that configures the controller:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: viking-controller
spec:
  # ---
  template:
    spec:
      # ...
      args:
      - -log-level=DEBUG
      # ...
```

```
$ k8s-ingress --help
Usage of ./k8s-ingress:
  -class string
    	value of the Ingress annotation kubernetes.io/ingress.class
    	the controller only considers Ingresses with this value for the
    	annotation (default "varnish")
  -devmode
    	enable development mode
  -incompleteRetryDelay duration
    	re-queue delay when the controller does not have all of the
    	information required for a necessary cluster change
    	must be > 0s (default 5s)
  -kubeconfig string
    	config path for the cluster master URL, for out-of-cluster runs
  -log-level string
    	log level: one of PANIC, FATAL, ERROR, WARN, INFO, DEBUG, 
    	or TRACE (default "INFO")
  -masterurl string
    	cluster master URL, for out-of-cluster runs
  -metricsport uint
    	port at which to listen for the /metrics endpoint (default 8080)
  -monitorintvl duration
    	interval at which the monitor thread checks and updates
    	instances of Varnish that implement Ingress.
    	Monitor deactivated when <= 0s (default 30s)
  -namespace string
    	namespace in which to listen for resources (default all)
  -readyfile string
    	path of a file to touch when the controller is ready,
    	for readiness probes
  -resyncPeriod duration
    	if non-zero, re-update the controller with the state of
    	the cluster this often, even if nothing has changed,
    	to synchronize state that may have been missed (default 30s)
  -version
    	print version and exit
```

``-kubeconfig`` and ``-masterurl`` can be used to run the controller
out-of-cluster:

```
$ k8s-ingress --kubeconfig $HOME/.kube/config
$ k8s-ingress -masterurl=https://192.168.0.100:8443
```

Out-of-cluster runs are mainly useful for quick tests during
development, for example with minikube, to skip the steps of
re-building and re-deploying the container image. These options should
not be used to run the controller in-cluster.

``-namespace ns`` restricts the controller to the namespace ``ns`` --
it only watches for Ingresses, Services and so on in the given
namespace. This may be necessary, for example, to deploy the
controller in an environment in which you do not have the
authorization to set up [RBAC](/deploy) so that the controller can run
in ``kube-system`` and watch all namespaces. See the
[examples](/examples/namespace) for a full example of a
single-namespace configuration. The controller watches all namespaces
by default.

If ``-readyfile /path/to/file`` is set, then the controller removes
the file at that path immediately at startup, if any exists, and
touches it when it is ready. Readiness probes can then test the file
for existence. By default, no readiness file is created.

``-class ingclass`` sets the string ``ingclass`` (default ``varnish``)
as the required value of the Ingress annotation
``kubernetes.io/ingress.class``.  The controller ignores Ingresses
that do not have the annotation set to this value. This makes it
possible for the Varnish Ingress implementation to co-exist in a
cluster with other implementations, as long as the other
implementations also respect the annotation. It also makes it possible
to deploy more than one Varnish controller to manage Varnish Services
and Ingresses separately; see the
[documentation](/docs/ref-svcs-ingresses-ns.md) and
[examples](/examples/architectures/multi-controller/) for details.

``-monitorintvl`` sets the interval for the
[monitor](/docs/monitor.md). By default 30 seconds, and the monitor is
deactivated for values <= 0. The monitor sleeps this long between
monitor runs for Varnish Services. See the documentation at the link
for more details.

``-resyncPeriod`` determines how often the controller is re-updated
with the state of the cluster. On this interval, the controller
receives Update events for all of the relevant resources in the
cluster, even if nothing has changed. This makes it possible to update
the state of Varnish Services, even if an earlier event was missed or
incorrectly interpreted for some reason. By default 30 seconds, and
there is no resync if set to 0s -- then the controller is only
notified when something changes.

``-incompleteRetryDelay`` sets an interval for the controller to wait
until re-attempting a synchronization that was not possible because
necessary information was missing. This is common, for example, for a
new Ingress deployment, where an IngressBacked is specified by its
Service name, but no Endpoints are currently defined for the Service,
because the Pods are not yet ready. But it may indicate that required
configuration was been omitted. Since the missing information is
commonly corrected after a few seconds, this delay allows the
controller to wait without too many rapid and unsuccessful retries.
If the interval is too long, deployments may take unnecessarily
long. If it is too short, the controller may spin through too many
unsuccessful retries. noisily generating error messages in the log and
warning Events. Default is 5s; MUST be > 0s.

``-metricsport`` (default 8080) sets the port number at which the
controller listens for the HTTP endpoint ``/metrics`` to publish
[metrics](/docs/ref-metrics.md) that are suitable for integration with
[Prometheus](https://prometheus.io/docs/introduction/overview/). It
must match the value of the ``containerPort`` configured for ``http``
in the [Pod template](/deploy/controller.yaml) for the controller
(cf. the [deplyoment instructions](/deploy#deploy-the-controller)).

``-log-level`` sets the log level for the main controller code,
``INFO`` by default.

``-devmode`` enables development mode. The
[``TemplateConfig`` Custom Resoure](/docs/ref-template-cfg.md) is
only read by the controller when this flag is set. Not intended for
production use.

``-version`` prints the controller version and exits. ``-help`` prints
the usage message shown above and exits.
