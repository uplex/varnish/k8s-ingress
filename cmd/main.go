/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package main

//go:generate gogitversion -p main

import (
	"flag"
	"fmt"
	"math"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	clientset "code.uplex.de/uplex-varnish/k8s-ingress/pkg/client/clientset/versioned"
	vcr_informers "code.uplex.de/uplex-varnish/k8s-ingress/pkg/client/informers/externalversions"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/controller"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/haproxy"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/varnish"

	"github.com/sirupsen/logrus"

	api_v1 "k8s.io/api/core/v1"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

const (
	defIncomplRetryDelay = 5 * time.Second
	defIngressClass      = "viking.uplex.de/ingress-controller"
)

var (
	versionF = flag.Bool("version", false, "print version and exit")
	loglvlF  = flag.String("log-level", "INFO",
		"log level: one of PANIC, FATAL, ERROR, WARN, INFO, DEBUG, \n"+
			"or TRACE")
	namespaceF = flag.String("namespace", api_v1.NamespaceAll,
		"namespace in which to listen for resources (default all)")
	masterURLF = flag.String("masterurl", "", "cluster master URL, for "+
		"out-of-cluster runs")
	kubeconfigF = flag.String("kubeconfig", "", "config path for the "+
		"cluster master URL, for out-of-cluster runs")
	readyfileF = flag.String("readyfile", "", "path of a file to touch "+
		"when the controller is ready,\nfor readiness probes")
	monIntvlF = flag.Duration("monitorintvl", 30*time.Second,
		"interval at which the monitor thread checks and updates\n"+
			"instances of Varnish that implement Ingress.\n"+
			"Monitor deactivated when <= 0s")
	metricsPortF = flag.Uint("metricsport", 8080,
		"port at which to listen for the /metrics endpoint")
	ingressClassF = flag.String("class", defIngressClass,
		"IngressClass controller identifier\n"+
			"the controller only considers Ingresses that\n"+
			"specify an IngressClass with this value in its\n"+
			"controller field, unless that IngressClass is also\n"+
			"specified as the cluster default")
	resyncPeriodF = flag.Duration("resyncPeriod", 30*time.Second,
		"if non-zero, re-update the controller with the state of\n"+
			"the cluster this often, even if nothing has changed,\n"+
			"to synchronize state that may have been missed")
	incomplRetryDelayF = flag.Duration("incompleteRetryDelay",
		defIncomplRetryDelay,
		"re-queue delay when the controller does not have all of the\n"+
			"information required for a necessary cluster change\n"+
			"must be > 0s")
	devModeF     = flag.Bool("devmode", false, "enable development mode")
	varnishImplF = flag.String("varnishImpl", "",
		"set to 'klarlack' to enable features only implemented by\n"+
			"the klarlack image for Varnish Ingress")
	maxSyncRetriesF = flag.Uint("maxSyncRetries", 0,
		"maximum number of retires for cluster synchronizations\n"+
			"that fail due to recoverable errors, or because\n"+
			"necessary information is missing. 0 for unlimited\n"+
			"retries (default 0)")

	logFormat = logrus.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	}
	log = &logrus.Logger{
		Out:       os.Stdout,
		Formatter: &logFormat,
		Level:     logrus.InfoLevel,
	}
	informerStop = make(chan struct{})
)

const vikingSecretSelector = "viking.uplex.de/secret"

// The next two functions satisify type TweakListOptionsFunc in
// k8s.io/client-go/informers/internalinterfaces, for use in the
// creation of SharedInformerFactories below. These are used to filter
// Secret informers, so that we only read Secrets that are relevant to
// the Ingress application.

// Filters Secrets with labels that identify this Ingress.
func vikingSecrets(opts *meta_v1.ListOptions) {
	opts.LabelSelector = vikingSecretSelector
}

// Filters Secrets with the type field that identify a Secret to be
// used for TLS certificates, and named in an Ingress resource.
func ingressTLSSecrets(opts *meta_v1.ListOptions) {
	opts.FieldSelector = fields.OneTermEqualSelector("type",
		string(api_v1.SecretTypeTLS)).String()
}

func main() {
	flag.Parse()

	if *versionF {
		fmt.Printf("%s version %s\n", os.Args[0], version)
		os.Exit(0)
	}

	if *readyfileF != "" {
		if err := os.Remove(*readyfileF); err != nil && !os.IsNotExist(err) {
			fmt.Printf("Cannot remove ready file %s: %v",
				*readyfileF, err)
			os.Exit(-1)
		}
	}

	lvl := strings.ToLower(*loglvlF)
	switch lvl {
	case "panic":
		log.Level = logrus.PanicLevel
	case "fatal":
		log.Level = logrus.FatalLevel
	case "error":
		log.Level = logrus.ErrorLevel
	case "warn":
		log.Level = logrus.WarnLevel
	case "debug":
		log.Level = logrus.DebugLevel
	case "trace":
		log.Level = logrus.TraceLevel
	case "info":
		break
	default:
		fmt.Printf("Unknown log level %s, exiting", *loglvlF)
		os.Exit(-1)
	}

	if *ingressClassF == "" {
		log.Fatalf("class may not be empty")
		os.Exit(-1)
	}

	if *metricsPortF > math.MaxUint16 {
		log.Fatalf("metricsport %d out of range (max %d)",
			*metricsPortF, math.MaxUint16)
		os.Exit(-1)
	}

	if *incomplRetryDelayF == 0 {
		log.Fatal("incompleteRetryDelay must be > 0s")
		os.Exit(-1)
	}

	log.Info("Starting Varnish Ingress controller version:", version)
	log.Info("Ingress class:", *ingressClassF)
	if *devModeF {
		log.Info("devmode enabled")
	}
	if *maxSyncRetriesF > 0 {
		log.Infof("maxSyncRetries = %d", *maxSyncRetriesF)
	}

	log.Info("Die Würde des Menschen ist unantastbar")
	log.Info("Black Lives Matter")
	log.Info("Wolność mediów = demokracja! " +
		"Walczmy o wolność mediów w Polsce. " +
		"Zostały już tylko nieliczne media niezależne od władz. " +
		"Polska ciągle spada w światowym rankingu wolności prasy: " +
		"https://rsf.org/en/ranking Nie uciszajcie wolnych mediów!")

	vController := varnish.NewVarnishController(log, *monIntvlF)
	hController := haproxy.NewOffloaderController(log, *monIntvlF)

	config, err := clientcmd.BuildConfigFromFlags(*masterURLF, *kubeconfigF)
	if err != nil {
		log.Fatalf("error creating client configuration: %v", err)
	}
	kubeClient, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatal("Failed to create client:", err)
	}
	vingClient, err := clientset.NewForConfig(config)
	if err != nil {
		log.Fatal("Failed to create client:", err)
	}

	var informerFactory informers.SharedInformerFactory
	var vcrInformerFactory vcr_informers.SharedInformerFactory
	if *namespaceF == api_v1.NamespaceAll {
		informerFactory = informers.NewSharedInformerFactory(
			kubeClient, *resyncPeriodF)
		vcrInformerFactory = vcr_informers.NewSharedInformerFactory(
			vingClient, *resyncPeriodF)
	} else {
		vcrInformerFactory =
			vcr_informers.NewSharedInformerFactoryWithOptions(
				vingClient, *resyncPeriodF,
				vcr_informers.WithNamespace(*namespaceF))

		informerFactory = informers.NewSharedInformerFactoryWithOptions(
			kubeClient, *resyncPeriodF,
			informers.WithNamespace(*namespaceF))
	}
	vsecrInformerFactory := informers.NewSharedInformerFactoryWithOptions(
		kubeClient, *resyncPeriodF,
		informers.WithNamespace(*namespaceF),
		informers.WithTweakListOptions(vikingSecrets))
	tsecrInformerFactory := informers.NewSharedInformerFactoryWithOptions(
		kubeClient, *resyncPeriodF,
		informers.WithNamespace(*namespaceF),
		informers.WithTweakListOptions(ingressTLSSecrets))

	ingController, err := controller.NewIngressController(log,
		*ingressClassF, *namespaceF, *devModeF, *varnishImplF,
		kubeClient, vController, hController, informerFactory,
		vcrInformerFactory, vsecrInformerFactory, tsecrInformerFactory,
		*incomplRetryDelayF, *maxSyncRetriesF)
	if err != nil {
		log.Fatalf("Could not initialize controller: %v", err)
		os.Exit(-1)
	}
	vController.EvtGenerator(ingController)
	go handleTermination(log, ingController, vController, hController)
	vController.Start()
	hController.Start()
	informerFactory.Start(informerStop)
	ingController.Run(*readyfileF, uint16(*metricsPortF), *devModeF)
}

func handleTermination(
	log *logrus.Logger,
	ingc *controller.IngressController,
	vc *varnish.Controller,
	hc *haproxy.Controller) {

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM, syscall.SIGINT)

	sig := <-signalChan
	log.Infof("Received signal (%s), shutting down", sig.String())

	log.Info("Shutting down the ingress controller")
	ingc.Stop()

	log.Info("Shutting down informers")
	informerStop <- struct{}{}

	log.Info("Shutting down the haproxy controller")
	hc.Quit()

	log.Info("Shutting down the Varnish controller")
	vc.Quit()

	log.Info("Exiting")
	os.Exit(0)
}
