FROM golang:1.21 as builder

RUN apt-get update -q && \
    apt-get install -y golang-golang-x-tools pandoc

RUN git clone https://github.com/slimhazard/gogitversion && \
    cd gogitversion && \
    chmod 755 bootstrap.sh && \
    make install

RUN mkdir -p /go/src/code.uplex.de/uplex-varnish/k8s-ingress/cmd
RUN mkdir -p /go/src/code.uplex.de/uplex-varnish/k8s-ingress/pkg
RUN mkdir -p /go/src/code.uplex.de/uplex-varnish/k8s-ingress/.git
WORKDIR /go/src/code.uplex.de/uplex-varnish/k8s-ingress
COPY go.mod .
COPY go.sum .

RUN go mod download
RUN go mod verify

COPY ./pkg/ /go/src/code.uplex.de/uplex-varnish/k8s-ingress/pkg/
COPY ./cmd/ /go/src/code.uplex.de/uplex-varnish/k8s-ingress/cmd/
COPY ./.git/ /go/src/code.uplex.de/uplex-varnish/k8s-ingress/.git/

RUN go generate ./cmd/... && go build ./pkg/... ./cmd/... && \
    CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -o vikingctrl cmd/*.go

FROM alpine:3.19.1
ENV USER=controller UID=10001

RUN adduser --disabled-password --gecos "viking controller" \
	--home "/nonexistent" --shell "/sbin/nologin" --no-create-home \
        --uid "${UID}" \
        "${USER}"

COPY --from=builder /go/src/code.uplex.de/uplex-varnish/k8s-ingress/vikingctrl /usr/bin/

USER controller:controller
ENTRYPOINT ["/usr/bin/vikingctrl"]
