vcl 4.1;

include "bogo_backend.vcl";

sub vcl_recv {
	if (local.socket == "k8s") {
		if (req.url == "/ready") {
			return (synth(65200));
		}
		return (synth(65404));
	}
	if (local.socket == "vk8s_cfg") {
		return (vcl(vk8s_configured));
	}
	return (vcl(vk8s_regular));
}

sub vcl_synth {
	if (resp.status == 65200 || resp.status == 65404) {
		return (deliver);
	}
}
