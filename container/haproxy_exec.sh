#!/bin/bash

# XXX observed a bug (not properly understood as of now) where
# some component causes haproxy to go haywire for too large a
# ulimit.
# the offending value seems be between 1<<25 and 1<<26
#
# 1<<25
ulimit -n 33554432 || true

set -e
set -u

/bin/sed -e "s/%%SECRET_DATAPLANEAPI%%/${SECRET_DATAPLANEAPI}/g" -e "s/%%POD_NAMESPACE%%/${POD_NAMESPACE}/g" /etc/haproxy/haproxy.cfg > /run/haproxy/haproxy.cfg

if [[ ${EUID} > 0 ]]; then
    /bin/sed -i -e '\:chroot /run/offload:d' /run/haproxy/haproxy.cfg
fi

exec /usr/sbin/haproxy -f /run/haproxy/haproxy.cfg "$@"
