vcl 4.1;

include "bogo_backend.vcl";

# Send a synthetic response with status 200 to every request.
# Used for the configured check when Varnish is configured to implement
# an Ingress.
sub vcl_recv {
	return(synth(200));
}

# Empty response body.
sub vcl_synth {
	return (deliver);
}
