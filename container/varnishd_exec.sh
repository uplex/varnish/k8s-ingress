#!/bin/bash

set -e
set -u

exec /usr/sbin/varnishd -F -a :${HTTP_PORT},${PROTO} -a k8s=:${READY_PORT} \
     -a k8s_offload=${OFFLOAD_PATH},PROXY,group=${GROUP},mode=0660         \
     -a vk8s_cfg=:${CONFIG_PORT} -i vk8s_${POD_NAMESPACE}_${POD_NAME}      \
     -S ${SECRET_PATH}/${SECRET_FILE} -T 0.0.0.0:${ADMIN_PORT}             \
     -p vcl_path=/etc/varnish -I /etc/varnish/start.cli	-f '' "$@"
