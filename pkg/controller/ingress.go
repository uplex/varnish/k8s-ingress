/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

// Methods for syncing Ingresses

import (
	"context"
	"encoding/base64"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	vcr_v1alpha1 "code.uplex.de/uplex-varnish/k8s-ingress/pkg/apis/varnishingress/v1alpha1"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/haproxy"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/update"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/varnish"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/varnish/vcl"

	"k8s.io/client-go/tools/cache"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"

	api_v1 "k8s.io/api/core/v1"
	net_v1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	ingressClassKey  = "kubernetes.io/ingress.class"
	annotationPrefix = "ingress.varnish-cache.org/"
	varnishSvcKey    = annotationPrefix + "varnish-svc"
	defACLcomparand  = "client.ip"
	defACLfailStatus = uint16(403)
	defMax2ndTTL     = "5m"
	defStickTblSz    = 128
	defMaxConn       = 2000
	extOnldInstances = 3
	vikingPubSvcKey  = vikingLabelPfx + "svc"
	vikingPubSvcVal  = "public"
)

var (
	vikingPubSvcSet = labels.Set(map[string]string{
		vikingPubSvcKey: vikingPubSvcVal,
	})
	// Selector for use in List() operations to find Services that
	// expose the public Ingress ports. To get address for use in
	// the Ingress status.loadBalancer field.
	vikingPubSvcSelector = labels.SelectorFromSet(vikingPubSvcSet)
)

func (worker *NamespaceWorker) filterVarnishIngSvcs(
	svcs []*api_v1.Service,
) []*api_v1.Service {
	n := 0
	for _, svc := range svcs {
		if worker.isVarnishIngSvc(svc) {
			svcs[n] = svc
			n++
		}
	}
	svcs = svcs[:n]
	return svcs
}

func (worker *NamespaceWorker) getVarnishSvcForIng(
	ing *net_v1.Ingress,
) (*api_v1.Service, error) {
	svcs, err := worker.listers.svc.List(varnishIngressSelector)
	if err != nil {
		return nil, err
	}
	worker.log.Debugf("Cluster Services: %+v", svcs)
	svcs = worker.filterVarnishIngSvcs(svcs)
	worker.log.Debugf("Cluster Varnish Services for Ingress: %+v", svcs)

	if varnishSvc, exists := ing.Annotations[varnishSvcKey]; exists {
		worker.log.Debugf("Ingress %s/%s has annotation %s:%s",
			ing.Namespace, ing.Name, varnishSvcKey, varnishSvc)
		targetNs, targetSvc, err := cache.SplitMetaNamespaceKey(varnishSvc)
		if err != nil {
			return nil, err
		}
		if targetNs == "" {
			targetNs = worker.namespace
		}
		for _, svc := range svcs {
			if svc.Namespace == targetNs && svc.Name == targetSvc {
				return svc, nil
			}
		}
		worker.log.Debugf("Ingress %s/%s: Varnish Service %s not found",
			ing.Namespace, ing.Name, varnishSvc)
		return nil, nil
	}
	worker.log.Debugf("Ingress %s/%s does not have annotation %s",
		ing.Namespace, ing.Name, varnishSvcKey)
	if len(svcs) == 1 {
		worker.log.Debugf("Exactly one Varnish Ingress Service "+
			"cluster-wide: %s", svcs[0])
		return svcs[0], nil
	}
	svcs, err = worker.svc.List(varnishIngressSelector)
	if err != nil {
		return nil, err
	}
	worker.log.Debugf("Namespace Services: %+v", svcs)
	svcs = worker.filterVarnishIngSvcs(svcs)
	worker.log.Debugf("Namespace Varnish Services for Ingress: %+v", svcs)
	if len(svcs) == 1 {
		worker.log.Debugf("Exactly one Varnish Ingress Service "+
			"in namespace %s: %s", worker.namespace, svcs[0])
		return svcs[0], nil
	}
	return nil, nil
}

func (worker *NamespaceWorker) getIngsForVarnishSvc(
	svc *api_v1.Service,
) ([]*net_v1.Ingress, error) {
	ings, err := worker.listers.ing.List(labels.Everything())
	if err != nil {
		return nil, err
	}
	allVarnishSvcs, err := worker.listers.svc.List(varnishIngressSelector)
	if err != nil {
		return nil, err
	}
	allVarnishSvcs = worker.filterVarnishIngSvcs(allVarnishSvcs)
	nsVarnishSvcs, err := worker.svc.List(varnishIngressSelector)
	if err != nil {
		return nil, err
	}
	nsVarnishSvcs = worker.filterVarnishIngSvcs(nsVarnishSvcs)

	ings4Svc := make([]*net_v1.Ingress, 0)
	for _, ing := range ings {
		if isViking, err := worker.isVikingIngress(ing); err != nil {
			return nil, err
		} else if !isViking {
			continue
		}
		namespace := ing.Namespace
		if namespace == "" {
			namespace = "default"
		}
		if ingSvc, exists := ing.Annotations[varnishSvcKey]; exists {
			targetNs, targetSvc, err := cache.SplitMetaNamespaceKey(ingSvc)
			if err != nil {
				return nil, err
			}
			if targetNs == "" {
				targetNs = namespace
			}
			if targetNs == svc.Namespace && targetSvc == svc.Name {
				ings4Svc = append(ings4Svc, ing)
			}
		} else if len(allVarnishSvcs) == 1 {
			ings4Svc = append(ings4Svc, ing)
		} else if ing.Namespace == svc.Namespace &&
			len(nsVarnishSvcs) == 1 {

			ings4Svc = append(ings4Svc, ing)
		}
	}
	return ings4Svc, nil
}

func ingMergeError(ings []*net_v1.Ingress) error {
	host2ing := make(map[string]*net_v1.Ingress)
	var ingWdefBackend *net_v1.Ingress
	for _, ing := range ings {
		if ing.Spec.DefaultBackend != nil {
			if ingWdefBackend != nil {
				return fmt.Errorf("default backend configured "+
					"in more than one Ingress: %s/%s and "+
					"%s/%s", ing.Namespace, ing.Name,
					ingWdefBackend.Namespace,
					ingWdefBackend.Name)
			}
			ingWdefBackend = ing
		}
		for _, rule := range ing.Spec.Rules {
			if otherIng, exists := host2ing[rule.Host]; exists {
				return fmt.Errorf("host '%s' named in rules "+
					"for more than one Ingress: %s/%s and "+
					"%s/%s", rule.Host, otherIng.Namespace,
					otherIng.Name, ing.Namespace, ing.Name)
			}
			host2ing[rule.Host] = ing
		}
	}
	return nil
}

func (worker *NamespaceWorker) ingBackend2Addrs(
	namespace string,
	backend net_v1.IngressBackend,
) (addrs []vcl.Address, extName string, extPort string, status update.Status) {
	nsLister := worker.listers.svc.Services(namespace)

	svc, err := nsLister.Get(backend.Service.Name)
	if err != nil {
		status = IncompleteIfNotFound(err, "%v", err)
		return
	}

	if svc.Spec.Type == api_v1.ServiceTypeExternalName {
		if svc.Spec.ExternalName == "" {
			return addrs, extName, extPort,
				update.MakeFatal(
					"Service %s/%s: ExternalName is empty "+
						"for Service Type externalName",
					svc.Namespace, svc.Name)
		}
		extName = svc.Spec.ExternalName
		if len(svc.Spec.Ports) > 0 {
			idx := 0
			for i, port := range svc.Spec.Ports {
				if port.Name == "http" {
					idx = i
					break
				}
			}
			extPort = svc.Spec.Ports[idx].TargetPort.String()
		} else {
			if backend.Service.Port.Number != 0 {
				extPort = strconv.Itoa(int(
					backend.Service.Port.Number))
			} else {
				extPort = backend.Service.Port.Name
			}
		}
		return
	}

	endps, err := worker.getServiceEndpoints(svc)
	if endps == nil || err != nil {
		if endps == nil {
			status = update.MakeIncomplete(
				"could not find endpoints for service: %s/%s",
				svc.Namespace, svc.Name)
		} else {
			status = IncompleteIfNotFound(err,
				"Error getting endpoints for service %v: %v",
				svc, err)
		}
		return
	}

	targetPort := int32(0)
	ingSvcPort := backend.Service.Port
	for _, port := range svc.Spec.Ports {
		if (ingSvcPort.Number != 0 && port.Port == ingSvcPort.Number) ||
			(ingSvcPort.Number == 0 &&
				port.Name == ingSvcPort.Name) {
			targetPort, err = worker.
				getTargetPort(&port, svc)
			if err != nil {
				status = IncompleteIfNotFound(err,
					"Error determining target "+
						"port for port %v in Ingress: "+
						"%v", ingSvcPort, err)
				return
			}
			break
		}
	}
	if targetPort == 0 {
		status = update.MakeFatal(
			"No port %v in service %s/%s", ingSvcPort,
			svc.Namespace, svc.Name)
		return
	}

	addrs, status = endpsTargetPort2Addrs(svc, endps, targetPort)
	if len(addrs) == 0 || addrs == nil {
		status = update.MakeIncomplete(
			"Service %s/%s: no addresses found", svc.Namespace,
			svc.Name)
	}
	return
}

func getVCLProbe(probe *vcr_v1alpha1.ProbeSpec) *vcl.Probe {
	if probe == nil {
		return nil
	}
	vclProbe := &vcl.Probe{
		URL:      probe.URL,
		Request:  make([]string, len(probe.Request)),
		Timeout:  probe.Timeout,
		Interval: probe.Interval,
	}
	copy(vclProbe.Request, probe.Request)

	if probe.ExpResponse != nil {
		vclProbe.ExpResponse = uint16(*probe.ExpResponse)
	}
	if probe.Initial != nil {
		vclProbe.Initial = strconv.Itoa((int(*probe.Window)))
	}
	if probe.Window != nil {
		vclProbe.Window = strconv.Itoa((int(*probe.Window)))
	}
	if probe.Threshold != nil {
		vclProbe.Threshold = strconv.Itoa((int(*probe.Threshold)))
	}
	return vclProbe
}

func (worker *NamespaceWorker) getVCLSvc(
	svcNamespace string,
	svcName string,
	addrs []vcl.Address,
	extName string,
	extPort string,
) (
	vcl.Service,
	*vcr_v1alpha1.BackendConfig,
	*haproxy.OnloadSpec,
	update.Status,
) {
	if svcNamespace == "" {
		svcNamespace = "default"
	}
	vclSvc := vcl.Service{
		Name:               svcNamespace + "/" + svcName,
		Addresses:          addrs,
		ExternalName:       extName,
		ExternalPort:       extPort,
		FollowDNSRedirects: true,
	}
	nsLister := worker.listers.bcfg.BackendConfigs(svcNamespace)
	bcfgs, err := nsLister.List(labels.Everything())
	if err != nil {
		if errors.IsNotFound(err) {
			status := update.MakeNoop(
				"No BackendConfig in namespace %s",
				svcNamespace)
			return vclSvc, nil, nil, status
		}
		return vclSvc, nil, nil, update.MakeRecoverable("%v", err)
	}
	var bcfg *vcr_v1alpha1.BackendConfig
BCfgs:
	for _, b := range bcfgs {
		// XXX report error if > 1 BackendConfig for the Service
		for _, svc := range b.Spec.Services {
			if svc == svcName {
				bcfg = b
				break BCfgs
			}
		}
	}
	if bcfg == nil {
		return vclSvc, nil, nil,
			update.MakeNoop("Service %s/%s: no BackendConfig "+
				"specified", svcNamespace, svcName)
	}
	if bcfg.Spec.Director != nil {
		if extName != "" {
			return vclSvc, nil, nil,
				update.MakeFatal(
					"Service %s/%s, BackendConfig %s/%s: "+
						"director may not be set for "+
						"an ExternalName Service",
					svcNamespace, svcName,
					bcfg.Namespace, bcfg.Name)
		}
		vclSvc.Director = &vcl.Director{
			Type: vcl.GetDirectorType(
				string(bcfg.Spec.Director.Type)),
			Rampup:     bcfg.Spec.Director.Rampup,
			ShardParam: bcfg.Spec.Director.ShardParam,
		}
		if bcfg.Spec.Director.Warmup != nil {
			vclSvc.Director.Warmup = float64(*bcfg.Spec.Director.Warmup) / 100.0
		}
	}
	// XXX if bcfg.Spec.Probe == nil, look for a HTTP readiness check
	// in the ContainerSpec.
	vclSvc.Probe = getVCLProbe(bcfg.Spec.Probe)
	vclSvc.HostHeader = bcfg.Spec.HostHeader
	vclSvc.ConnectTimeout = bcfg.Spec.ConnectTimeout
	vclSvc.FirstByteTimeout = bcfg.Spec.FirstByteTimeout
	vclSvc.BetweenBytesTimeout = bcfg.Spec.BetweenBytesTimeout
	vclSvc.DomainUsageTimeout = bcfg.Spec.DomainUsageTimeout
	vclSvc.FirstLookupTimeout = bcfg.Spec.FirstLookupTimeout
	vclSvc.ResolverIdleTimeout = bcfg.Spec.ResolverIdleTimeout
	vclSvc.ResolverTimeout = bcfg.Spec.ResolverTimeout
	if bcfg.Spec.DNSRetryDelay != "" {
		vclSvc.DNSRetryDelay = bcfg.Spec.DNSRetryDelay
	}
	if bcfg.Spec.MaxConnections != nil {
		vclSvc.MaxConnections = uint32(*bcfg.Spec.MaxConnections)
	}
	if bcfg.Spec.ProxyHeader != nil {
		vclSvc.ProxyHeader = uint8(*bcfg.Spec.ProxyHeader)
	}
	if bcfg.Spec.MaxDNSQueries != nil {
		vclSvc.MaxDNSQueries = uint16(*bcfg.Spec.MaxDNSQueries)
	}
	if bcfg.Spec.FollowDNSRedirects != nil {
		vclSvc.FollowDNSRedirects = *bcfg.Spec.FollowDNSRedirects
	}
	if bcfg.Spec.NameTTL != nil {
		vclSvc.NameTTL = &vcl.NameTTLSpec{}
		if bcfg.Spec.NameTTL.TTL != nil {
			vclSvc.NameTTL.TTL = *bcfg.Spec.NameTTL.TTL
		}
		switch bcfg.Spec.NameTTL.From {
		case vcr_v1alpha1.FromCfg:
			vclSvc.NameTTL.From = vcl.FromCfg
		case vcr_v1alpha1.FromDNS:
			vclSvc.NameTTL.From = vcl.FromDNS
		case vcr_v1alpha1.FromMin:
			vclSvc.NameTTL.From = vcl.FromMin
		case vcr_v1alpha1.FromMax:
			vclSvc.NameTTL.From = vcl.FromMax
		}
	}

	var onload *haproxy.OnloadSpec
	if bcfg.Spec.TLS != nil {
		if worker.varnishImpl != "klarlack" {
			return vclSvc, nil, nil, update.MakeFatal(
				"Service %s/%s, BackendConfig %s/%s: "+
					"TLS onload only enabled for the "+
					"klarlack implementation "+
					"(varnishImpl: %s)",
				svcNamespace, svcName, bcfg.Namespace,
				bcfg.Name, worker.varnishImpl)
		}

		vclSvc.Via = true

		onload = &haproxy.OnloadSpec{
			Verify:     true,
			StickTblSz: defStickTblSz,
			MaxConn:    defMaxConn,
		}
		if bcfg.Spec.TLS.MaxConn != nil {
			onload.MaxConn = int(*bcfg.Spec.TLS.MaxConn)
		}
		if bcfg.Spec.TLS.StickTblSz != nil {
			onload.StickTblSz = int(*bcfg.Spec.TLS.StickTblSz)
		}

		if bcfg.Spec.TLS.Authority != nil {
			authority := *bcfg.Spec.TLS.Authority
			if authority != "" {
				if vclSvc.HostHeader != "" &&
					vclSvc.HostHeader != authority {
					return vclSvc, nil, nil,
						update.MakeFatal(
							"Service %s/%s, "+
								"BackendConfig %s/%s: "+
								"host-header (%s) and "+
								"authority (%s) conflict",
							svcNamespace, svcName,
							bcfg.Namespace,
							bcfg.Name,
							vclSvc.HostHeader,
							authority)
				}
				vclSvc.HostHeader = authority
			}
			vclSvc.Authority = &authority
			onload.Authority = true
		} else {
			vclSvc.Authority = nil
			onload.Authority = false
		}

		if bcfg.Spec.TLS.Verify != nil && !*bcfg.Spec.TLS.Verify {
			onload.Verify = false
		} else if bcfg.Spec.TLS.CACrt != "" {
			// XXX
			// - find CACrt in cert-manager.io/v1, same ns
			//	- Sync Incomplete if not found
			// - else check isCA field
			//	- SyncFatal if false
			// - else find Secret in secretName field, same ns
			//	- Sync Incomplete if not found
			// 3. else add to haproxySpec
			// will have to send to crt-dnldr
			if bcfg.Spec.TLS.CAIssuer != "" {
				// XXX
				// 1. find in cert-manager, may be a cluster
				//    issuer if ns/name is specified
				// 2. SyncIncomplete if not found
				// 3. otherwise attempt verification
				// 4. SyncFatal if verfication fails
				// - not added to any config
			}
		} else if bcfg.Spec.TLS.CASecret != "" {
			// XXX
			// 1. Find in the same ns
			// 2. SyncIncomplete if not found
			// 3. otherwise update haproxy spec
			// will have to send to crt-dnldr
		} else {
			// SyncFatal, Verify true but no crt
		}
	}
	return vclSvc, bcfg, onload, update.MakeSuccess("")
}

func (worker *NamespaceWorker) ings2VCLSpec(ings []*net_v1.Ingress) (
	vcl.Spec,
	map[string]*vcr_v1alpha1.BackendConfig,
	map[string]*haproxy.OnloadSpec,
	[]update.Status,
) {
	vclSpec := vcl.Spec{}
	vclSpec.IntSvcs = make(map[string]vcl.Service)
	vclSpec.ExtSvcs = make(map[string]vcl.Service)
	bcfgs := make(map[string]*vcr_v1alpha1.BackendConfig)
	onlds := make(map[string]*haproxy.OnloadSpec)
	allStatus := []update.Status{}

	for _, ing := range ings {
		namespace := ing.Namespace
		if namespace == "" {
			namespace = "default"
		}
		if ing.Spec.DefaultBackend != nil {
			if vclSpec.DefaultService.Name != "" {
				panic("More than one Ingress default backend")
			}
			backend := ing.Spec.DefaultBackend
			addrs, extName, extPort, status := worker.
				ingBackend2Addrs(namespace, *backend)
			if status.IsError() {
				status.Obj = ing
				allStatus = append(allStatus, status)
				continue
			}
			vclSvc, bcfg, onload, status := worker.
				getVCLSvc(namespace, backend.Service.Name,
					addrs, extName, extPort)
			if status.IsError() {
				status.Obj = ing
				allStatus = append(allStatus, status)
				return vclSpec, bcfgs, onlds, allStatus
			}
			vclSpec.DefaultService = vclSvc
			key := namespace + "/" + backend.Service.Name
			if extName == "" {
				vclSpec.IntSvcs[key] = vclSvc
				if onload != nil {
					onload.Instances = len(addrs)
				}
			} else {
				vclSpec.ExtSvcs[key] = vclSvc
				if onload != nil {
					onload.Instances = extOnldInstances
				}
			}
			if bcfg != nil {
				bcfgs[vclSvc.Name] = bcfg
			}
			if onload != nil {
				onlds[vclSvc.Name] = onload
			}
		}
		for _, rule := range ing.Spec.Rules {
			vclRule := vcl.Rule{Host: rule.Host}
			vclRule.PathMap = make(map[vcl.PathKey]vcl.Service)
			if rule.HTTP == nil {
				vclSpec.Rules = append(vclSpec.Rules, vclRule)
				continue
			}
			for _, path := range rule.HTTP.Paths {
				addrs, extName, extPort, status := worker.
					ingBackend2Addrs(namespace,
						path.Backend)

				if status.IsError() {
					status.Obj = ing
					allStatus = append(allStatus, status)
					return vclSpec, bcfgs, onlds, allStatus
				}
				if extName == "" &&
					(len(addrs) == 0) {
					panic("len(addrs)==0 namespace=" +
						namespace + " IngressBackend=" +
						path.Backend.String())
				}
				vclSvc, bcfg, onload, status := worker.
					getVCLSvc(namespace,
						path.Backend.Service.Name, addrs,
						extName, extPort)
				if status.IsError() {
					status.Obj = ing
					allStatus = append(allStatus, status)
					return vclSpec, bcfgs, onlds, allStatus
				}
				if path.PathType == nil {
					panic("pathType==nil namespace=" +
						namespace + " Ingress=" +
						ing.Name)
				}
				pathKey := vcl.PathKey{Path: path.Path}
				switch *path.PathType {
				case net_v1.PathTypeExact:
					pathKey.Type = vcl.PathExact
				case net_v1.PathTypePrefix:
					pathKey.Type = vcl.PathPrefix
				case net_v1.PathTypeImplementationSpecific:
					pathKey.Type = vcl.PathImplSpecific
				default:
					panic("Illegal pathType namespace=" +
						namespace + " Ingress=" +
						ing.Name)
				}
				_, exists := vclRule.PathMap[pathKey]
				if exists {
					msg := fmt.Sprintf("Ingress %s/%s: "+
						"more than one rule with "+
						"path %s and type %s for "+
						"host %s",
						namespace, ing.Name,
						pathKey.Path, *path.PathType,
						rule.Host)
					status = update.MakeFatal(msg)
					status.Obj = ing
					allStatus = append(allStatus, status)
					return vclSpec, bcfgs, onlds, allStatus
				}
				vclRule.PathMap[pathKey] = vclSvc
				key := namespace + "/" +
					path.Backend.Service.Name
				if extName == "" {
					vclSpec.IntSvcs[key] = vclSvc
					if onload != nil {
						onload.Instances = len(addrs)
					}
				} else {
					vclSpec.ExtSvcs[key] = vclSvc
					if onload != nil {
						onload.Instances = extOnldInstances
					}
				}
				if bcfg != nil {
					bcfgs[vclSvc.Name] = bcfg
				}
				if onload != nil {
					onlds[vclSvc.Name] = onload
				}
			}
			vclSpec.Rules = append(vclSpec.Rules, vclRule)
		}
		status := update.MakeSuccess("")
		status.Obj = ing
		allStatus = append(allStatus, status)
	}
	return vclSpec, bcfgs, onlds, allStatus
}

func configConditions(vclConds []vcl.MatchTerm,
	vcfgConds []vcr_v1alpha1.Condition,
) {
	if len(vclConds) != len(vcfgConds) {
		panic("configConditions: unequal slice lengths")
	}
	for i, cond := range vcfgConds {
		vclMatch := vcl.MatchTerm{
			Comparand: cond.Comparand,
			Value:     cond.Value,
		}
		vclMatch.Compare, vclMatch.Negate = configComparison(cond.Compare)
		vclConds[i] = vclMatch
	}
}

func (worker *NamespaceWorker) configAuth(spec *vcl.Spec,
	vcfg *vcr_v1alpha1.VarnishConfig,
) error {
	if len(vcfg.Spec.Auth) == 0 {
		worker.log.Infof("No Auth spec found for VarnishConfig %s/%s",
			vcfg.Namespace, vcfg.Name)
		return nil
	}
	worker.log.Tracef("VarnishConfig %s/%s: configure %d VCL auths",
		vcfg.Namespace, vcfg.Name, len(vcfg.Spec.Auth))
	spec.Auths = make([]vcl.Auth, 0, len(vcfg.Spec.Auth))
	for _, auth := range vcfg.Spec.Auth {
		worker.log.Tracef("VarnishConfig %s/%s configuring VCL auth "+
			"from: %+v", vcfg.Namespace, vcfg.Name, auth)
		secret, err := worker.vsecr.Get(auth.SecretName)
		if err != nil {
			return err
		}
		if len(secret.Data) == 0 {
			worker.log.Warnf("No secrets found in Secret %s/%s "+
				"for realm %s in VarnishConfig %s/%s, ignoring",
				secret.Namespace, secret.Name, auth.Realm,
				vcfg.Namespace, vcfg.Name)
			continue
		}
		worker.log.Tracef("VarnishConfig %s/%s configure %d "+
			"credentials for realm %s", vcfg.Namespace, vcfg.Name,
			len(secret.Data), auth.Realm)
		vclAuth := vcl.Auth{
			Realm:       auth.Realm,
			Credentials: make([]string, 0, len(secret.Data)),
			Conditions:  make([]vcl.MatchTerm, len(auth.Conditions)),
			UTF8:        auth.UTF8,
		}
		if auth.Type == "" || auth.Type == vcr_v1alpha1.Basic {
			vclAuth.Status = vcl.Basic
		} else {
			vclAuth.Status = vcl.Proxy
		}
		for user, pass := range secret.Data {
			str := user + ":" + string(pass)
			cred := base64.StdEncoding.EncodeToString([]byte(str))
			worker.log.Tracef("VarnishConfig %s/%s: add cred %s "+
				"for realm %s to VCL config", vcfg.Namespace,
				vcfg.Name, cred, vclAuth.Realm)
			vclAuth.Credentials = append(vclAuth.Credentials, cred)
		}
		configConditions(vclAuth.Conditions, auth.Conditions)
		worker.log.Tracef("VarnishConfig %s/%s add VCL auth config: "+
			"%+v", vcfg.Namespace, vcfg.Name, vclAuth)
		spec.Auths = append(spec.Auths, vclAuth)
	}
	return nil
}

func (worker *NamespaceWorker) configACL(spec *vcl.Spec,
	vcfg *vcr_v1alpha1.VarnishConfig,
) {
	if len(vcfg.Spec.ACLs) == 0 {
		worker.log.Infof("No ACL spec found for VarnishConfig %s/%s",
			vcfg.Namespace, vcfg.Name)
		return
	}
	spec.ACLs = make([]vcl.ACL, len(vcfg.Spec.ACLs))
	for i, acl := range vcfg.Spec.ACLs {
		worker.log.Infof("VarnishConfig %s/%s configuring ACL %s",
			vcfg.Namespace, vcfg.Name, acl.Name)
		worker.log.Tracef("ACL %s: %+v", acl.Name, acl)
		vclACL := vcl.ACL{
			Name:       acl.Name,
			Addresses:  make([]vcl.ACLAddress, len(acl.Addresses)),
			Conditions: make([]vcl.MatchTerm, len(acl.Conditions)),
		}
		if acl.Comparand == "" {
			vclACL.Comparand = defACLcomparand
		} else {
			vclACL.Comparand = acl.Comparand
		}
		if acl.ACLType == "" || acl.ACLType == vcr_v1alpha1.Whitelist {
			vclACL.Whitelist = true
		}
		if acl.FailStatus == nil {
			vclACL.FailStatus = defACLfailStatus
		} else {
			vclACL.FailStatus = uint16(*acl.FailStatus)
		}
		for j, addr := range acl.Addresses {
			vclAddr := vcl.ACLAddress{
				Addr:   addr.Address,
				Negate: addr.Negate,
			}
			if addr.MaskBits == nil {
				vclAddr.MaskBits = vcl.NoMaskBits
			} else {
				vclAddr.MaskBits = uint8(*addr.MaskBits)
			}
			vclACL.Addresses[j] = vclAddr
		}
		configConditions(vclACL.Conditions, acl.Conditions)
		if acl.ResultHdr != nil {
			worker.log.Tracef("ACL %s: ResultHdr=%+v", acl.Name,
				*acl.ResultHdr)
			vclACL.ResultHdr.Header = acl.ResultHdr.Header
			vclACL.ResultHdr.Success = acl.ResultHdr.Success
			vclACL.ResultHdr.Failure = acl.ResultHdr.Failure
		}
		worker.log.Tracef("VarnishConfig %s/%s add VCL ACL config: "+
			"%+v", vcfg.Namespace, vcfg.Name, vclACL)
		spec.ACLs[i] = vclACL
	}
}

func (worker *NamespaceWorker) configRewrites(spec *vcl.Spec,
	vcfg *vcr_v1alpha1.VarnishConfig,
) error {
	if len(vcfg.Spec.Rewrites) == 0 {
		worker.log.Infof("No rewrite spec found for VarnishConfig "+
			"%s/%s", vcfg.Namespace, vcfg.Name)
		return nil
	}
	spec.Rewrites = make([]vcl.Rewrite, len(vcfg.Spec.Rewrites))
	for i, rw := range vcfg.Spec.Rewrites {
		worker.log.Infof("VarnishConfig %s/%s: configuring rewrite "+
			"for target %s", vcfg.Namespace, vcfg.Name, rw.Target)
		worker.log.Tracef("Rewrite: %v", rw)
		vclRw := vcl.Rewrite{
			Target: rw.Target,
			Rules:  make([]vcl.RewriteRule, len(rw.Rules)),
		}
		for j := range rw.Rules {
			vclRw.Rules[j] = vcl.RewriteRule{
				Value:   rw.Rules[j].Value,
				Rewrite: rw.Rules[j].Rewrite,
			}
		}
		if rw.Source == "" {
			// XXX
			// The Source is the same as the Target if:
			// - Method is one of sub, suball or rewrite,
			// - or Method is one of replace, append or
			//   prepend, and there are either no rules
			//   or more than one rule.
			if rw.Method == vcr_v1alpha1.Sub ||
				rw.Method == vcr_v1alpha1.Suball ||
				rw.Method == vcr_v1alpha1.Rewrite ||
				((rw.Method == vcr_v1alpha1.Replace ||
					rw.Method == vcr_v1alpha1.Append ||
					rw.Method == vcr_v1alpha1.Prepend) &&
					len(rw.Rules) != 1) {

				vclRw.Source = rw.Target
			}
		} else {
			vclRw.Source = rw.Source
		}

		switch rw.Method {
		case vcr_v1alpha1.Replace:
			vclRw.Method = vcl.Replace
		case vcr_v1alpha1.Sub:
			vclRw.Method = vcl.Sub
		case vcr_v1alpha1.Suball:
			vclRw.Method = vcl.Suball
		case vcr_v1alpha1.Rewrite:
			vclRw.Method = vcl.RewriteMethod
		case vcr_v1alpha1.Append:
			vclRw.Method = vcl.Append
		case vcr_v1alpha1.Prepend:
			vclRw.Method = vcl.Prepend
		case vcr_v1alpha1.Delete:
			vclRw.Method = vcl.Delete
		default:
			return fmt.Errorf("illegal method %s", rw.Method)
		}

		if rw.Compare == "" {
			rw.Compare = vcr_v1alpha1.Match
		}
		vclRw.Compare, vclRw.Negate = configComparison(rw.Compare)

		switch rw.VCLSub {
		case vcr_v1alpha1.Recv:
			vclRw.VCLSub = vcl.Recv
		case vcr_v1alpha1.Pipe:
			vclRw.VCLSub = vcl.Pipe
		case vcr_v1alpha1.Pass:
			vclRw.VCLSub = vcl.Pass
		case vcr_v1alpha1.Hash:
			vclRw.VCLSub = vcl.Hash
		case vcr_v1alpha1.Purge:
			vclRw.VCLSub = vcl.Purge
		case vcr_v1alpha1.Miss:
			vclRw.VCLSub = vcl.Miss
		case vcr_v1alpha1.Hit:
			vclRw.VCLSub = vcl.Hit
		case vcr_v1alpha1.Deliver:
			vclRw.VCLSub = vcl.Deliver
		case vcr_v1alpha1.Synth:
			vclRw.VCLSub = vcl.Synth
		case vcr_v1alpha1.BackendFetch:
			vclRw.VCLSub = vcl.BackendFetch
		case vcr_v1alpha1.BackendResponse:
			vclRw.VCLSub = vcl.BackendResponse
		case vcr_v1alpha1.BackendError:
			vclRw.VCLSub = vcl.BackendError
		default:
			vclRw.VCLSub = vcl.Unspecified
		}

		switch rw.Select {
		case vcr_v1alpha1.Unique:
			vclRw.Select = vcl.Unique
		case vcr_v1alpha1.First:
			vclRw.Select = vcl.First
		case vcr_v1alpha1.Last:
			vclRw.Select = vcl.Last
		case vcr_v1alpha1.Exact:
			vclRw.Select = vcl.Exact
		case vcr_v1alpha1.Longest:
			vclRw.Select = vcl.Longest
		case vcr_v1alpha1.Shortest:
			vclRw.Select = vcl.Shortest
		default:
			vclRw.Select = vcl.Unique
		}

		if rw.MatchFlags != nil {
			vclRw.MatchFlags = configMatchFlags(*rw.MatchFlags)
		} else {
			vclRw.MatchFlags.CaseSensitive = true
		}
		spec.Rewrites[i] = vclRw
	}
	return nil
}

func (worker *NamespaceWorker) configReqDisps(spec *vcl.Spec,
	reqDisps []vcr_v1alpha1.RequestDispSpec, kind, namespace, name string,
) {
	if len(reqDisps) == 0 {
		worker.log.Infof("No request disposition specs found for %s "+
			"%s/%s", kind, namespace, name)
		return
	}
	worker.log.Infof("Configuring request dispositions for %s %s/%s",
		kind, namespace, name)
	spec.Dispositions = make([]vcl.DispositionSpec, len(reqDisps))
	for i, disp := range reqDisps {
		worker.log.Tracef("ReqDisposition: %+v", disp)
		vclDisp := vcl.DispositionSpec{
			Conditions: reqConds2vclConds(disp.Conditions),
		}
		for j, cond := range disp.Conditions {
			vclCond := vcl.Condition{
				Comparand: cond.Comparand,
			}
			if len(cond.Values) > 0 {
				vclCond.Values = make([]string, len(cond.Values))
				copy(vclCond.Values, cond.Values)
			}
			if cond.Count != nil {
				count := uint(*cond.Count)
				vclCond.Count = &count
			}
			vclCond.Compare, vclCond.Negate = configComparison(cond.Compare)
			if cond.MatchFlags != nil {
				vclCond.MatchFlags = configMatchFlags(
					*cond.MatchFlags)
			} else {
				vclCond.MatchFlags.CaseSensitive = true
			}
			vclDisp.Conditions[j] = vclCond
		}
		vclDisp.Disposition.Action = vcl.RecvReturn(
			disp.Disposition.Action)
		if disp.Disposition.Action == vcr_v1alpha1.RecvSynth {
			vclDisp.Disposition.Status = uint16(
				*disp.Disposition.Status)
			vclDisp.Disposition.Reason = disp.Disposition.Reason
		}
		spec.Dispositions[i] = vclDisp
	}
}

func (worker *NamespaceWorker) ings2OffloaderSpec(
	svc *api_v1.Service,
	ings []*net_v1.Ingress,
) (haproxy.Spec, error) {
	offldrSpec := haproxy.Spec{
		Namespace: svc.Namespace,
		Name:      svc.Name,
		Secrets:   make([]haproxy.SecretSpec, 0),
		Onload:    make(map[string]haproxy.OnloadSpec),
	}
	for _, ing := range ings {
		for _, tls := range ing.Spec.TLS {
			if len(tls.Hosts) > 0 {
				worker.log.Warnf("Ingress %s/%s: ignoring "+
					"Hosts config %v",
					ing.Namespace,
					ing.Name, tls.Hosts)
			}
			if tls.SecretName == "" {
				return offldrSpec, fmt.Errorf("ingress %s/%s: "+
					"TLS without Secret not supported",
					ing.Namespace,
					ing.Name)
			}
			secr := haproxy.SecretSpec{
				Namespace: ing.Namespace,
				Name:      tls.SecretName,
			}
			offldrSpec.Secrets = append(offldrSpec.Secrets, secr)
		}
	}
	return offldrSpec, nil
}

func ingsArrayString(ings []*net_v1.Ingress) string {
	ingStrings := make([]string, len(ings))
	for i, ing := range ings {
		ingStrings[i] = ing.Namespace + "/" + ing.Name
	}
	return strings.Join(ingStrings, " ")
}

// Update the status.loadBalancer field of each successfully synced
// Ingress, using network addresses of the Services that expose the
// http and https ports.
func (worker *NamespaceWorker) updateIngStatus(
	admSvc *api_v1.Service, ings []*net_v1.Ingress,
) update.Status {
	svcs, err := worker.svc.List(vikingPubSvcSelector)
	if err != nil {
		if errors.IsNotFound(err) {
			worker.log.Warnf("No Service found in namespace %s "+
				"with label %s=%s, will not update "+
				"status.loadBalancer for Ingress(es): %s",
				worker.namespace, vikingPubSvcKey,
				vikingPubSvcVal, ingsArrayString(ings))
			return update.MakeSuccess("")
		}
		return update.MakeFatal(
			"Error searching for Services with label %s=%s in "+
				"namespace %s, cannot update "+
				"status.loadBalancer for Ingress(es) %s: %v",
			vikingPubSvcKey, vikingPubSvcVal, worker.namespace,
			ingsArrayString(ings), err)
	}
	if len(svcs) == 0 {
		worker.log.Warnf("No Service found in namespace %s "+
			"with label %s=%s, will not update "+
			"status.loadBalancer for Ingress(es): %s",
			worker.namespace, vikingPubSvcKey, vikingPubSvcVal,
			ingsArrayString(ings))
		return update.MakeSuccess("")
	}

	ips := map[string]struct{}{}
	hosts := map[string]struct{}{}
	for _, svc := range svcs {
		if !labels.Equals(svc.Spec.Selector, admSvc.Spec.Selector) {
			continue
		}
		switch svc.Spec.Type {
		case api_v1.ServiceTypeClusterIP:
			if svc.Spec.ClusterIP != "" {
				ips[svc.Spec.ClusterIP] = struct{}{}
			}
		case api_v1.ServiceTypeNodePort:
			for _, extIP := range svc.Spec.ExternalIPs {
				ips[extIP] = struct{}{}
			}
			if svc.Spec.ClusterIP != "" {
				ips[svc.Spec.ClusterIP] = struct{}{}
			}
		case api_v1.ServiceTypeLoadBalancer:
			for _, ing := range svc.Status.LoadBalancer.Ingress {
				if ing.IP != "" {
					ips[ing.IP] = struct{}{}
				}
				if ing.Hostname != "" {
					hosts[ing.Hostname] = struct{}{}
				}
			}
			if svc.Spec.LoadBalancerIP != "" {
				ips[svc.Spec.LoadBalancerIP] = struct{}{}
			}
		case api_v1.ServiceTypeExternalName:
			if svc.Spec.ExternalName != "" {
				hosts[svc.Spec.ExternalName] = struct{}{}
			}
		default:
			worker.log.Warnf("Service %s/%s has label %s=%s but "+
				"type %s, not adding to status.loadBalancer "+
				"for Ingress(es): %s", svc.Namespace, svc.Name,
				vikingPubSvcKey, vikingPubSvcVal, svc.Spec.Type,
				ingsArrayString(ings))
		}
	}
	if len(ips)+len(hosts) == 0 {
		worker.log.Warnf("No public addresses found for Services in "+
			"namespace %s with label %s=%s, will not update "+
			"status.loadBalancer for Ingress(es): %s",
			worker.namespace, vikingPubSvcKey, vikingPubSvcVal,
			ingsArrayString(ings))
		return update.MakeSuccess("")
	}

	lb := make([]net_v1.IngressLoadBalancerIngress, len(ips)+len(hosts))
	n := 0
	for host := range hosts {
		lb[n].Hostname = host
		n++
	}
	for ip := range ips {
		lb[n].IP = ip
		n++
	}

	updateOpts := metav1.UpdateOptions{}
	for _, ing := range ings {
		ctx := context.TODO()
		ing.Status.LoadBalancer.Ingress = make([]net_v1.IngressLoadBalancerIngress,
			len(ips)+len(hosts))
		copy(ing.Status.LoadBalancer.Ingress, lb)
		ingClient := worker.client.NetworkingV1().
			Ingresses(ing.Namespace)
		_, err := ingClient.UpdateStatus(ctx, ing, updateOpts)
		if err == nil {
			continue
		}
		// Commonly fails because the ResourceVersion has been updated.
		// Retry if necessary with a fresh copy from the API server.
		freshIng, err := ingClient.Get(ctx, ing.Name,
			metav1.GetOptions{})
		if err != nil {
			return update.MakeFatal(
				"Cannot update status for Ingress %s/%s, "+
					"retry fetch failed: %v",
				ing.Namespace, ing.Name, err)
		}
		if !reflect.DeepEqual(ing.Status.LoadBalancer,
			freshIng.Status.LoadBalancer) {
			freshIng.Status.LoadBalancer = ing.Status.LoadBalancer
			_, err = ingClient.UpdateStatus(ctx, freshIng,
				updateOpts)
			if err != nil {
				return update.MakeFatal(
					"Cannot update status for Ingress "+
						"%s/%s: %v",
					ing.Namespace, ing.Name, err)
			}
		}
	}
	return update.MakeSuccess("")
}

func (worker *NamespaceWorker) addOrUpdateIng(
	ing *net_v1.Ingress,
) []update.Status {
	ingKey := ing.Namespace + "/" + ing.Name
	worker.log.Infof("Adding or Updating Ingress: %s", ingKey)

	// Get the Varnish Service and its Pods
	svc, err := worker.getVarnishSvcForIng(ing)
	if err != nil {
		return []update.Status{IncompleteIfNotFound(err, "%v", err)}
	}
	if svc == nil {
		return []update.Status{update.MakeIncomplete("No Varnish Service found "+"for Ingress %s/%s", ing.Namespace, ing.Name)}
	}
	svcKey := svc.Namespace + "/" + svc.Name
	worker.log.Infof("Ingress %s configured for Varnish Service %s", ingKey,
		svcKey)

	ings, err := worker.getIngsForVarnishSvc(svc)
	if err != nil {
		return []update.Status{IncompleteIfNotFound(err, "%v", err)}
	}
	if len(ings) == 0 {
		worker.log.Infof("No Ingresses to be implemented by Varnish "+
			"Service %s", svcKey)
		worker.log.Infof("Setting Service %s to not ready", svcKey)
		if err := worker.vController.SetNotReady(svcKey); err != nil {
			return []update.Status{update.MakeRecoverable("%v", err)}
		}
		if worker.hController.HasOffloader(svcKey) {
			worker.log.Infof("Deleting haproxy config for %s",
				svcKey)
			if status := worker.hController.
				DeleteOffldSvc(svcKey); status.IsError() {
				return []update.Status{status}
			}
		} else {
			worker.log.Infof("No haproxy config found for %s",
				svcKey)
		}
		return []update.Status{update.MakeNoop("")}
	}

	ingNames := make([]string, len(ings))
	for i, ingress := range ings {
		ingNames[i] = ingress.Namespace + "/" + ingress.Name
	}
	worker.log.Infof("Ingresses implemented by Varnish Service %s: %v",
		svcKey, ingNames)
	vclSpec, bcfgs, onlds, allStatus := worker.ings2VCLSpec(ings)

	if update.HasOnlyErrors(allStatus) {
		return allStatus
	}

	worker.log.Tracef("VCL spec generated from the Ingresses: %v", vclSpec)

	var vcfg *vcr_v1alpha1.VarnishConfig
	var haproxyDefSpec haproxy.DefaultsSpec
	worker.log.Tracef("Listing VarnishConfigs in namespace %s",
		worker.namespace)
	vcfgs, err := worker.vcfg.List(labels.Everything())
	if err != nil {
		if !errors.IsNotFound(err) {
			allStatus = append(allStatus, update.MakeRecoverable("%v", err))
			return allStatus
		}
	}
	for _, v := range vcfgs {
		worker.log.Tracef("VarnishConfig: %s/%s: %+v", v.Namespace,
			v.Name, v)
		for _, svcName := range v.Spec.Services {
			if svcName == svc.Name {
				vcfg = v
				break
			}
		}
	}
	if vcfg != nil {
		worker.log.Infof("Found VarnishConfig %s/%s for Varnish "+
			"Service %s/%s", vcfg.Namespace, vcfg.Name,
			svc.Namespace, svc.Name)
		if status := worker.configSharding(
			&vclSpec, vcfg, svc); status.IsError() {
			allStatus = append(allStatus, status)
			return allStatus
		}
		if err := worker.configAuth(&vclSpec, vcfg); err != nil {
			allStatus = append(allStatus, update.MakeIncomplete("%v", err))
			return allStatus
		}
		worker.configACL(&vclSpec, vcfg)
		if err := worker.configRewrites(&vclSpec, vcfg); err != nil {
			allStatus = append(allStatus, update.MakeFatal("%v", err))
			return allStatus
		}
		worker.configReqDisps(&vclSpec, vcfg.Spec.ReqDispositions,
			vcfg.Kind, vcfg.Namespace, vcfg.Name)
		worker.configVikingTLS(&haproxyDefSpec, vcfg.Spec.TLS)
		vclSpec.VCL = vcfg.Spec.VCL
	} else {
		worker.log.Infof("Found no VarnishConfigs for Varnish Service "+
			"%s/%s", svc.Namespace, svc.Name)
	}

	// Update any TLS offloader first. Offloaders forward their
	// readiness check to the Varnish readiness check, so if we're
	// adding a new service, the offloader becomes ready when
	// Varnish is ready.
	offldrSpec, err := worker.ings2OffloaderSpec(svc, ings)
	if err != nil {
		allStatus = append(allStatus, update.MakeFatal("%v", err))
		return allStatus
	}
	offldrSpec.Defaults = &haproxyDefSpec
	for n, v := range onlds {
		offldrSpec.Onload[n] = *v
	}
	if offldrSpec.Name == "" && len(onlds) == 0 {
		worker.log.Infof("No TLS config found for Ingresses: %v",
			ingNames)
	} else {
		if secrKey, dSecr, err := worker.getDplaneSecret(); err != nil {
			allStatus = append(allStatus, update.MakeIncomplete("%s", err))
			return allStatus
		} else if dSecr == nil {
			worker.log.Warnf("Service %s: Currently no known "+
				"dataplane Secret", svcKey)
		} else {
			worker.log.Infof("Service %s: setting dataplane Secret"+
				" %s", svcKey, secrKey)
			worker.hController.SetDataplaneSecret(secrKey, dSecr)
			worker.hController.SetOffldSecret(svcKey, secrKey)
		}
	}

	addrs, offldAddrs, serviceStatus := worker.svc2Addrs(svc)
	if serviceStatus.IsError() {
		allStatus = append(allStatus, serviceStatus)
		return allStatus
	}

	if len(offldrSpec.Secrets) != 0 || len(onlds) != 0 {
		for sidx, tlsSecr := range offldrSpec.Secrets {
			var s *api_v1.Secret
			if s, err = worker.tsecr.Get(tlsSecr.Name); err != nil {
				return []update.Status{IncompleteIfNotFound(err, "%v", err)}
			}
			offldrSpec.Secrets[sidx].UID = string(s.UID)
			offldrSpec.Secrets[sidx].ResourceVersion = s.ResourceVersion
		}
		if status := worker.hController.Update(svcKey, offldAddrs,
			offldrSpec); status.IsError() {
			allStatus = append(allStatus, status)
			return allStatus
		}
		for _, tlsSecr := range offldrSpec.Secrets {
			status := worker.hController.AddOrUpdateTLSSecret(
				svcKey, tlsSecr)
			if status.IsError() {
				allStatus = append(allStatus, status)
				return allStatus
			}
			worker.log.Infof("Ingress TLS Secret %s/%s: "+
				"Service %s: %s", tlsSecr.Namespace,
				tlsSecr.Name, tlsSecr, status)
		}
	}

	secret, err := worker.setAdmSecretForSvc(svc)
	if err != nil {
		allStatus = append(allStatus, IncompleteIfNotFound(err, "%v", err))
		return allStatus
	}
	secretKey := secret.Namespace + "/" + secret.Name
	updateSvcStatus := worker.vController.UpdateSvcForSecret(svcKey, secretKey)
	if updateSvcStatus.Type == update.Fatal {
		// Only abort on fatal errors.
		// Recoverable or Incomplete status may be
		// resolved by the upcoming update.
		allStatus = append(allStatus, updateSvcStatus)
		return allStatus
	}
	worker.hController.SetOffldSecret(svcKey, secretKey)

	ingsMeta := make(map[string]varnish.Meta)
	for _, ing := range ings {
		metaDatum := varnish.Meta{
			Key: ing.Namespace + "/" + ing.Name,
			UID: string(ing.UID),
			Ver: ing.ResourceVersion,
		}
		ingsMeta[metaDatum.Key] = metaDatum
	}
	var vcfgMeta varnish.Meta
	if vcfg != nil {
		vcfgMeta = varnish.Meta{
			Key: vcfg.Namespace + "/" + vcfg.Name,
			UID: string(vcfg.UID),
			Ver: vcfg.ResourceVersion,
		}
	}
	bcfgMeta := make(map[string]varnish.Meta)
	for name, bcfg := range bcfgs {
		bcfgMeta[name] = varnish.Meta{
			Key: bcfg.Namespace + "/" + bcfg.Name,
			UID: string(bcfg.UID),
			Ver: bcfg.ResourceVersion,
		}
	}
	worker.log.Debugf("Check if config is loaded: hash=%s "+
		"ingressMetaData=%+v vcfgMetaData=%+v bcfgMetaData=%+v",
		vclSpec.Canonical().DeepHash(), ingsMeta, vcfgMeta, bcfgMeta)
	if worker.vController.HasConfig(svcKey, addrs, vclSpec, ingsMeta,
		vcfgMeta, bcfgMeta) {
		worker.log.Infof("Varnish Service %s: config already "+
			"loaded: hash=%s", svcKey,
			vclSpec.Canonical().DeepHash())
	} else {
		worker.log.Debugf("Update config svc=%s ingressMetaData=%+v "+
			"vcfgMetaData=%+v bcfgMetaData=%+v: %+v", svcKey,
			ingsMeta, vcfgMeta, bcfgMeta, vclSpec)
		controllerStatus := worker.vController.Update(svcKey, vclSpec, addrs,
			ingsMeta, vcfgMeta, bcfgMeta)
		if controllerStatus.IsError() {
			allStatus = append(allStatus, controllerStatus)
			return allStatus
		}
		worker.log.Debugf("Updated config svc=%s ingressMetaData=%+v "+
			"vcfgMetaData=%+v bcfgMetaData=%+v: %+v", svcKey,
			ingsMeta, vcfgMeta, bcfgMeta, vclSpec)
	}

	allStatus = append(allStatus, worker.updateIngStatus(svc, ings))
	return allStatus
}

// We only handle Ingresses whose IngressClass is set to the value
// given in the "class" CLI option (default
// "viking.uplex.de/ingress-controller").  Unless that IngressClass
// has been specified as the cluster default, in which case we also
// handle Ingresses without any ingress class specification.
//
// For backward compatibility, we give precedence to the deprecated
// ingress.class annotation. The annotation should not be used for new
// projects, and support will be removed in a future version.
func (worker *NamespaceWorker) isVikingIngress(
	ing *net_v1.Ingress,
) (bool, error) {
	class, exists := ing.Annotations[ingressClassKey]
	if exists {
		return class == worker.ingClass, nil
	}
	if ing.Spec.IngressClassName == nil ||
		*ing.Spec.IngressClassName == "" {
		isDefault, err := worker.isVikingDefaultIngressClass()
		if err != nil {
			return false, err
		}
		return isDefault, nil
	}
	name, err := worker.vikingIngressClassName()
	if err != nil {
		return false, err
	}
	if name == "" {
		return false, fmt.Errorf("no IngressClass found with "+
			"spec.controller: %s", worker.ingClass)
	}
	return *ing.Spec.IngressClassName == name, nil
}

func (worker *NamespaceWorker) chkAddOrUpdateIng(
	ing *net_v1.Ingress,
) []update.Status {
	if isViking, err := worker.isVikingIngress(ing); err != nil {
		return []update.Status{update.MakeRecoverable(
			"Error checking if Ingress %s/%s is to be "+
				"implemented by viking: %v", ing.Namespace,
			ing.Name, err)}
	} else if !isViking {
		var name string
		syncCounters.WithLabelValues(worker.namespace, "Ingress",
			"Ignore").Inc()
		if ing.Spec.IngressClassName != nil {
			name = *ing.Spec.IngressClassName
		} else {
			name = "nil"
		}
		return []update.Status{update.MakeNoop("Ignoring Ingress %s/%s, "+
			"IngressClass '%s' does not specify '%s'",
			ing.Namespace, ing.Name, name, worker.ingClass)}
	}
	return worker.addOrUpdateIng(ing)
}

func (worker *NamespaceWorker) syncIng(key string) []update.Status {
	nsKey := worker.namespace + "/" + key
	worker.log.Info("Syncing Ingress:", nsKey)
	ing, err := worker.ing.Get(key)
	if err != nil {
		return []update.Status{IncompleteIfNotFound(err, "Cannot Get Ingress %s: %v",
			nsKey, err)}
	}
	return worker.chkAddOrUpdateIng(ing)
}

func (worker *NamespaceWorker) addIng(key string) []update.Status {
	return worker.syncIng(key)
}

func (worker *NamespaceWorker) updateIng(key string) []update.Status {
	return worker.syncIng(key)
}

func (worker *NamespaceWorker) deleteIng(obj interface{}) []update.Status {
	ing, ok := obj.(*net_v1.Ingress)
	if !ok || ing == nil {
		return []update.Status{update.MakeNoop("Delete Ingress: not found: %v", obj)}
	}
	return worker.chkAddOrUpdateIng(ing)
}
