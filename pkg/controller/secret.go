/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	"fmt"

	vcr_v1alpha1 "code.uplex.de/uplex-varnish/k8s-ingress/pkg/apis/varnishingress/v1alpha1"
	api_v1 "k8s.io/api/core/v1"
	net_v1 "k8s.io/api/networking/v1"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/haproxy"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/update"
)

// XXX make these configurable
const (
	admSecretKey         = "admin"
	dplaneSecretKey      = "dataplaneapi"
	vikingSecretLabelKey = vikingLabelPfx + "secret"
	vikingAdmSecretKey   = vikingLabelPfx + "admSecret"
	vikingAdmSecretVal   = "admin"
	vikingAuthSecretVal  = "auth"
)

var (
	vikingAdmSecretSet = labels.Set(map[string]string{
		vikingSecretLabelKey: vikingAdmSecretVal,
	})
	// Selector for use in List() operations to find Secrets
	// designated as admin secrets for this application (Varnish
	// shared secret and dataplane API password for Basic Auth).
	vikingAdmSecretSelector = labels.SelectorFromSet(vikingAdmSecretSet)
)

func (worker *NamespaceWorker) getIngsForTLSSecret(
	secret *api_v1.Secret,
) (ings []*net_v1.Ingress, status update.Status) {
	nsIngs, err := worker.ing.List(labels.Everything())
	if err != nil {
		if errors.IsNotFound(err) {
			status = update.MakeNoop(
				"No Ingresses found in namespace %s",
				worker.namespace)
			return
		}
		return nil, update.MakeRecoverable("%v", err)
	}

	for _, ing := range nsIngs {
		if isViking, err := worker.isVikingIngress(ing); err != nil {
			return nil, update.MakeRecoverable(
				"Error checking if Ingress %s/%s is to be "+
					"implemented by viking: %v",
				ing.Namespace, ing.Name, err)
		} else if !isViking {
			continue
		}
		for _, tls := range ing.Spec.TLS {
			if tls.SecretName == secret.Name {
				ings = append(ings, ing)
			}
		}
	}
	return ings, update.MakeSuccess("")
}

func (worker *NamespaceWorker) isVikingIngressTLSSecret(
	secret *api_v1.Secret) (bool, update.Status) {

	if ings, status := worker.getIngsForTLSSecret(secret); status.IsError() {
		return false, status
	} else if ings == nil || len(ings) == 0 {
		return false, update.MakeSuccess("")
	}
	return true, update.MakeSuccess("")
}

func (worker *NamespaceWorker) deleteTLSSecret(
	secret *api_v1.Secret,
) update.Status {
	ings, status := worker.getIngsForTLSSecret(secret)
	if status.IsError() || status.Type == update.Noop {
		return status
	}
	spec := haproxy.SecretSpec{
		Namespace: secret.Namespace,
		Name:      secret.Name,
	}
	for _, ing := range ings {
		svc, err := worker.getVarnishSvcForIng(ing)
		if err != nil {
			if errors.IsNotFound(err) {
				worker.log.Infof("No Service found for viking "+
					"Ingress %s/%s, no action for delete "+
					"TLS Secret %s/%s necessary",
					ing.Namespace, ing.Name,
					secret.Namespace, secret.Name)
				continue
			}
			return update.MakeRecoverable("%v", err)
		}
		key := svc.Namespace + "/" + svc.Name
		status := worker.hController.DeleteTLSSecret(key, spec)
		if status.IsError() {
			return status
		}
		worker.log.Infof("Deleting TLS Secret %s/%s for Ingress %s/%s,"+
			" Service %s/%s: %s", secret.Namespace, secret.Name,
			ing.Namespace, ing.Name, svc.Namespace, svc.Name,
			status)
	}
	return update.MakeSuccess(
		"TLS Secret %s/%s deleted for viking Services",
		secret.Namespace, secret.Name)
}

func (worker *NamespaceWorker) getVarnishSvcsForSecret(
	secretName string,
) ([]*api_v1.Service, error) {
	var secrSvcs []*api_v1.Service
	svcs, err := worker.svc.List(varnishIngressSelector)
	if err != nil {
		return secrSvcs, err
	}
	for _, svc := range svcs {
		if s, ok := svc.Annotations[vikingAdmSecretKey]; ok &&
			s == secretName {
			secrSvcs = append(secrSvcs, svc)
		}
	}
	return secrSvcs, nil
}

func (worker *NamespaceWorker) updateVcfgsForSecret(
	secrName string,
) update.Status {
	var vcfgs []*vcr_v1alpha1.VarnishConfig
	vs, err := worker.vcfg.List(labels.Everything())
	if err != nil {
		if errors.IsNotFound(err) {
			return update.MakeNoop(
				"Secret %s/%s: no VarnishConfigs in the same "+
					"namespace", worker.namespace,
				secrName)
		}
		return update.MakeRecoverable("%v", err)
	}
	for _, v := range vs {
		for _, auth := range v.Spec.Auth {
			if auth.SecretName == secrName {
				vcfgs = append(vcfgs, v)
			}
		}
	}
	if len(vcfgs) == 0 {
		return update.MakeNoop("No VarnishConfigs found for secret: "+
			"%s/%s", worker.namespace, secrName)
	}
	for _, vcfg := range vcfgs {
		worker.log.Infof("Requeuing VarnishConfig %s/%s "+
			"after update for secret %s/%s",
			vcfg.Namespace, vcfg.Name, worker.namespace, secrName)
		worker.queue.Add(&SyncObj{Type: Update, Obj: vcfg})
	}
	return update.MakeSuccess("Secret %s/%s: re-queued VarnishConfigs",
		worker.namespace, secrName)
}

func (worker *NamespaceWorker) updateVarnishSvcsForSecret(
	svcs []*api_v1.Service, secretKey string) update.Status {

	for _, svc := range svcs {
		svcKey := svc.Namespace + "/" + svc.Name
		if status := worker.vController.
			UpdateSvcForSecret(svcKey, secretKey); status.IsError() {

			return status
		}
	}
	return update.MakeSuccess("Secret %s: viking Services updated",
		secretKey)
}

func (worker *NamespaceWorker) enqueueIngsForTLSSecret(
	secret *api_v1.Secret) update.Status {

	ings, status := worker.getIngsForTLSSecret(secret)
	if status.IsError() {
		return status
	}
	if len(ings) == 0 {
		// syncCounters.WithLabelValues(worker.namespace, "Service",
		// 	"Ignore").Inc()
		return update.MakeNoop(
			"No Varnish Ingresses defined for TLS Secret %s/%s",
			secret.Namespace, secret.Name)
	}

	crt, crtOk := secret.Data["tls.crt"]
	key, keyOk := secret.Data["tls.key"]
	if !crtOk || !keyOk {
		return update.MakeFatal(
			"TLS Secret %s/%s missing required field",
			secret.Namespace, secret.Name)
	}
	if len(crt) == 0 || len(key) == 0 {
		return update.MakeFatal(
			"TLS Secret %s/%s required field is empty",
			secret.Namespace, secret.Name)
	}

	for _, ing := range ings {
		worker.log.Infof("Varnish Ingress %s/%s uses TLS Secret %s/%s,"+
			" re-queueing", ing.Namespace, ing.Name,
			secret.Namespace, secret.Name)
		worker.queue.Add(&SyncObj{Type: Update, Obj: ing})
	}
	return update.MakeSuccess(
		"Secret %s/%s: requeued Ingress(es)", secret.Namespace,
		secret.Name)
}

func (worker *NamespaceWorker) getDplaneSecret() (string, []byte, error) {
	secrets, err := worker.vsecr.List(vikingAdmSecretSelector)
	if err != nil {
		return "", nil, err
	}
	for _, secret := range secrets {
		data, exists := secret.Data[dplaneSecretKey]
		if !exists {
			continue
		}
		key := secret.Namespace + "/" + secret.Name
		return key, data, nil
	}
	return "", nil, nil
}

func (worker *NamespaceWorker) setSecret(secret *api_v1.Secret) error {
	secretData, exists := secret.Data[admSecretKey]
	if !exists {
		return fmt.Errorf("Secret %s/%s does not have key %s",
			secret.Namespace, secret.Name, admSecretKey)
	}
	secretKey := secret.Namespace + "/" + secret.Name
	worker.log.Tracef("Setting secret %s", secretKey)
	worker.vController.SetAdmSecret(secretKey, secretData)

	secretData, exists = secret.Data[dplaneSecretKey]
	if !exists {
		return nil
	}
	worker.hController.SetDataplaneSecret(secretKey, secretData)
	return nil
}

func (worker *NamespaceWorker) syncSecret(key string) update.Status {
	worker.log.Infof("Syncing Secret: %s/%s", worker.namespace, key)
	secret, err := worker.vsecr.Get(key)
	if err != nil {
		if secret, err = worker.tsecr.Get(key); err != nil {
			return IncompleteIfNotFound(err, "%v", err)
		}
	}

	if secret.Type == api_v1.SecretTypeTLS {
		return worker.enqueueIngsForTLSSecret(secret)
	}

	secretType, ok := secret.Labels[vikingSecretLabelKey]
	if !ok {
		// Should be impossible due to informer filtering
		return update.MakeFatal("Secret %s/%s lacks required label %s",
			secret.Namespace, secret.Name, vikingSecretLabelKey)
	}
	if secretType == vikingAuthSecretVal {
		return worker.updateVcfgsForSecret(secret.Name)
	}
	if secretType != vikingAdmSecretVal {
		return update.MakeFatal(
			"Secret %s/%s: unknown value %s for label %s",
			secret.Namespace, secret.Name, secretType,
			vikingSecretLabelKey)
	}

	svcs, err := worker.getVarnishSvcsForSecret(secret.Name)
	if err != nil {
		return IncompleteIfNotFound(err, "%v", err)
	}
	worker.log.Tracef("Found Varnish services for secret %s/%s: %v",
		secret.Namespace, secret.Name, svcs)
	if len(svcs) == 0 {
		return update.MakeNoop(
			"No Varnish services with admin secret: %s/%s",
			secret.Namespace, secret.Name)
	}

	err = worker.setSecret(secret)
	if err != nil {
		return update.MakeIncomplete("%v", err)
	}
	secretKey := secret.Namespace + "/" + secret.Name
	return worker.updateVarnishSvcsForSecret(svcs, secretKey)
}

func (worker *NamespaceWorker) addSecret(key string) update.Status {
	return worker.syncSecret(key)
}

func (worker *NamespaceWorker) updateSecret(key string) update.Status {
	return worker.syncSecret(key)
}

func (worker *NamespaceWorker) deleteSecret(obj interface{}) update.Status {
	secr, ok := obj.(*api_v1.Secret)
	if !ok || secr == nil {
		return update.MakeNoop("Delete Secret: not found: %v", obj)
	}

	if secr.Type == api_v1.SecretTypeTLS {
		if is, status := worker.
			isVikingIngressTLSSecret(secr); status.IsError() {
			return status
		} else if !is {
			return update.MakeNoop(
				"TLS Secret %s/%s not specified by any "+
					"Ingress for Varnish, ignoring",
				secr.Namespace, secr.Name)
		}
		worker.log.Infof("Deleting TLS Secret: %s/%s", secr.Namespace,
			secr.Name)
		return worker.deleteTLSSecret(secr)
	}

	secretType, ok := secr.Labels[vikingSecretLabelKey]
	if !ok {
		// Should be impossible due to informer filtering
		return update.MakeFatal("Secret %s/%s lacks required label %s",
			secr.Namespace, secr.Name, vikingSecretLabelKey)
	}
	if secretType == vikingAuthSecretVal {
		return worker.updateVcfgsForSecret(secr.Name)
	}
	if secretType != vikingAdmSecretVal {
		return update.MakeFatal(
			"Secret %s/%s: unknown value %s for label %s",
			secr.Namespace, secr.Name, secretType,
			vikingSecretLabelKey)
	}

	worker.log.Infof("Deleting Secret: %s/%s", secr.Namespace, secr.Name)
	svcs, err := worker.getVarnishSvcsForSecret(secr.Name)
	if err != nil {
		return IncompleteIfNotFound(err, "%v", err)
	}
	worker.log.Tracef("Found Varnish services for secret %s/%s: %v",
		secr.Namespace, secr.Name, svcs)
	if len(svcs) == 0 {
		return update.MakeNoop(
			"No Varnish services with admin secret: %s/%s",
			secr.Namespace, secr.Name)
	}

	secretKey := secr.Namespace + "/" + secr.Name
	worker.vController.DeleteAdmSecret(secretKey)
	worker.hController.DeleteDataplaneSecret(secretKey)

	return worker.updateVarnishSvcsForSecret(svcs, secretKey)
}
