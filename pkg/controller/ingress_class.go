/*
 * Copyright (c) 2021 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/update"

	net_v1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
)

const defIngClassKey = "ingressclass.kubernetes.io/is-default-class"

func (worker *NamespaceWorker) isVikingDefaultIngressClass() (bool, error) {
	ingClasses, err := worker.listers.ingcl.List(labels.Everything())
	if err != nil {
		if errors.IsNotFound(err) {
			return false, nil
		}
		return false, err
	}
	for _, ingClass := range ingClasses {
		isDefault, exists := ingClass.Annotations[defIngClassKey]
		if exists {
			if isDefault == "true" {
				return ingClass.Spec.Controller == worker.ingClass, nil
			} else if ingClass.Spec.Controller == worker.ingClass {
				return false, nil
			}
		} else if ingClass.Spec.Controller == worker.ingClass {
			return false, nil
		}
	}
	return false, nil
}

func (worker *NamespaceWorker) vikingIngressClassName() (string, error) {
	ingClasses, err := worker.listers.ingcl.List(labels.Everything())
	if err != nil {
		return "", err
	}
	for _, ingClass := range ingClasses {
		if ingClass.Spec.Controller == worker.ingClass {
			return ingClass.Name, nil
		}
	}
	return "", nil
}

func (worker *NamespaceWorker) syncIngClass(key string) update.Status {
	worker.log.Infof("Syncing IngressClass: %s", key)
	ingClass, err := worker.listers.ingcl.Get(key)
	if err != nil {
		return IncompleteIfNotFound(err, "%v", err)
	}
	//	worker.log.Tracef("IngressClass %s: %+v", ingClass.Name, ingClass)
	worker.log.Infof("IngressClass %s: %+v", ingClass.Name, ingClass)

	worker.log.Infof("IngressClass %s controller: %s", ingClass.Name,
		ingClass.Spec.Controller)
	if ingClass.Spec.Controller == worker.ingClass {
		worker.log.Infof("IngressClass %s is the viking controller "+
			"ingress class", ingClass.Name)
	}
	isDefault, exists := ingClass.Annotations[defIngClassKey]
	if exists && isDefault == "true" {
		worker.log.Infof("IngressClass %s is the cluster default "+
			"ingress class", ingClass.Name)
	}
	return update.MakeSuccess("")
}

func (worker *NamespaceWorker) addIngClass(key string) update.Status {
	return worker.syncIngClass(key)
}

func (worker *NamespaceWorker) updateIngClass(key string) update.Status {
	return worker.syncIngClass(key)
}

func (worker *NamespaceWorker) deleteIngClass(obj interface{}) update.Status {
	ingClass, ok := obj.(*net_v1.IngressClass)
	if !ok || ingClass == nil {
		return update.MakeNoop("Delete IngressClass: not found: %v",
			obj)
	}
	return update.MakeSuccess("")
}
