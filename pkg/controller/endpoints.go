/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	api_v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/labels"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/update"
)

func (worker *NamespaceWorker) syncEndp(key string) []update.Status {
	worker.log.Infof("Syncing Endpoints: %s/%s", worker.namespace, key)
	svc, err := worker.svc.Get(key)
	if err != nil {
		syncCounters.WithLabelValues(worker.namespace, "Endpoints",
			"Ignore").Inc()
		return []update.Status{update.MakeNoop(
			"Cannot get service for endpoints %s/%s, ignoring",
			worker.namespace, key)}
	}

	if worker.isVarnishIngSvc(svc) {
		worker.queue.Add(&SyncObj{Type: Update, Obj: svc})
		return []update.Status{update.MakeNoop("Endpoints changed for Varnish Ingress "+
			"service %s/%s, enqueuing service sync", svc.Namespace,
			svc.Name)}
	}

	worker.log.Tracef("Checking ingresses for endpoints: %s/%s",
		worker.namespace, key)
	ings, status := worker.getIngsForSvc(svc)
	if status.Type != update.Success {
		return []update.Status{status}
	}
	if len(ings) == 0 {
		syncCounters.WithLabelValues(worker.namespace, "Endpoints",
			"Ignore").Inc()
		return []update.Status{update.MakeNoop("No ingresses for endpoints: %s/%s",
			worker.namespace, key)}
	}

	worker.log.Tracef("Update ingresses for endpoints %s", key)
	requeued := make([]string, 0, len(ings))
	for _, ing := range ings {
		if isViking, err := worker.isVikingIngress(ing); err != nil {
			return []update.Status{update.MakeRecoverable(
				"Error checking if Ingress %s/%s is to be "+
					"implemented by viking: %v",
				ing.Namespace, ing.Name, err)}
		} else if !isViking {
			worker.log.Tracef("Ingress %s/%s: not Varnish",
				ing.Namespace, ing.Name)
			continue
		}
		allStatusFromIngress := worker.addOrUpdateIng(ing)
		if update.HasOnlyErrors(allStatusFromIngress) {
			return allStatusFromIngress
		}
		requeued = append(requeued, ing.Namespace+"/"+ing.Name)
	}
	if len(requeued) == 0 {
		return []update.Status{update.MakeNoop(
			"Endpoints %s/%s: not an IngressBackend for any "+
				"viking Ingress", svc.Namespace, svc.Name)}
	}
	return []update.Status{update.MakeSuccess(
		"Endpoints %s/%s: re-synced Ingresses for IngressBackend: %s",
		svc.Namespace, svc.Name, requeued)}
}

func (worker *NamespaceWorker) addEndp(key string) []update.Status {
	return worker.syncEndp(key)
}

func (worker *NamespaceWorker) updateEndp(key string) []update.Status {
	return worker.syncEndp(key)
}

func (worker *NamespaceWorker) deleteEndp(obj interface{}) []update.Status {
	endp, ok := obj.(*api_v1.Endpoints)
	if !ok || endp == nil {
		return []update.Status{update.MakeNoop("Delete Endpoints: not found: %v", obj)}
	}
	return worker.syncEndp(endp.Name)
}

// getAllServiceEndpoints returns the Endpoints of all Services in the
// same namespace that specify the same selectors.
func (worker *NamespaceWorker) getAllServiceEndpoints(
	svc *api_v1.Service,
) (epSlice []*api_v1.Endpoints, err error) {
	selector := svc.Spec.Selector
	nsLister := worker.listers.svc.Services(svc.Namespace)
	svcs, err := nsLister.List(labels.Everything())
	if err != nil {
		return
	}
	for _, nsSvc := range svcs {
		if !labels.Equals(selector, nsSvc.Spec.Selector) {
			continue
		}
		eps, err := worker.getServiceEndpoints(nsSvc)
		if err != nil {
			return epSlice, err
		}
		epSlice = append(epSlice, eps)
	}
	return
}

func getTargetPod(epAddr api_v1.EndpointAddress) (string, string) {
	ns := ""
	name := ""
	if epAddr.TargetRef != nil {
		ns = epAddr.TargetRef.Namespace
		name = epAddr.TargetRef.Name
	}
	return ns, name
}
