/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package haproxy

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
)

const (
	pemsPfx           = "/v1/pems/"
	uidParam          = "uid"
	versionParam      = "version"
	problemType       = "application/problem+json"
	errTypeInvalid    = "/errors/pems/invalidSecret"
	errTypePermission = "/errors/pems/write/permission"
)

// Problem Details object per RFC7807
type Problem struct {
	Type     string `json:"type"`
	Title    string `json:"title"`
	Status   int    `json:"status"`
	Detail   string `json:"detail"`
	Instance string `json:"instance"`
}

func (problem Problem) Error() string {
	return fmt.Sprintf("%d %s: %s: %s (type: %s, log instance: %s)",
		problem.Status, http.StatusText(problem.Status),
		problem.Title, problem.Detail, problem.Type, problem.Instance)
}

// IsNotFoundProblem returns true iff err is of type *Problem, and
// represents a Not Found error from the k8s-crt-dnldr REST API.
func IsNotFoundProblem(err error) bool {
	problem, ok := err.(*Problem)
	if !ok {
		return false
	}
	if problem.Status == http.StatusNotFound {
		return true
	}
	return false
}

// IsInvalidProblem returns true iff err is of type *Problem, and
// represents an error response from the k8s-crt-dnldr REST API that a
// TLS Secret is invalid -- one of the required fields "tls.crt" or
// "tls.key" is missing, or its value is empty.
func IsInvalidProblem(err error) bool {
	problem, ok := err.(*Problem)
	if !ok {
		return false
	}
	if problem.Type == errTypeInvalid {
		return true
	}
	return false
}

// IsPermissionProblem returns true iff err is of type *Problem, and
// represents an error response from the k8s-crt-dnldr REST API that
// there was a file permission error on writing or deleting the
// certificate for the TLS Secret.
func IsPermissionProblem(err error) bool {
	problem, ok := err.(*Problem)
	if !ok {
		return false
	}
	if problem.Type == errTypePermission {
		return true
	}
	return false
}

// CrtDnldrClient sends requests to the REST API of a k8s-crt-dnldr
// app, to instruct it to write or delete PEM files corresponding to
// TLS Secrets. This determines the certificates available to haproxy
// at configuration reload.
//
// See: https://code.uplex.de/k8s/k8s-crt-dnldr
type CrtDnldrClient struct {
	baseURL *url.URL
	rest    *http.Client
}

// NewCrtDnldrClient returns a client for the REST API listening at
// host for a k8s-crt-dnldr app.
//
// host may have the form "addr" or "addr:port", where addr may be a
// host name or IP address.
func NewCrtDnldrClient(host string) *CrtDnldrClient {
	return &CrtDnldrClient{
		baseURL: &url.URL{
			Scheme: "http",
			Host:   host,
		},
		rest: http.DefaultClient,
	}
}

func (client *CrtDnldrClient) getHost() string {
	return client.baseURL.Host
}

func (client *CrtDnldrClient) mkURL(spec SecretSpec) *url.URL {
	path := pemsPfx + spec.Namespace + "/" + spec.Name
	relPath := &url.URL{Path: path}
	url := client.baseURL.ResolveReference(relPath)
	if spec.UID != "" {
		url.Query().Set(uidParam, spec.UID)
	}
	if spec.ResourceVersion != "" {
		url.Query().Set(versionParam, spec.ResourceVersion)
	}
	return url
}

var (
	putSuccess = map[int]struct{}{
		http.StatusCreated:   {},
		http.StatusNoContent: {},
	}
	deleteSuccess = map[int]struct{}{
		http.StatusNoContent: {},
	}
)

func (client *CrtDnldrClient) mutate(
	spec SecretSpec, method string, successCodes map[int]struct{},
) error {
	url := client.mkURL(spec)
	req, err := http.NewRequest(method, url.String(), nil)
	if err != nil {
		return err
	}

	resp, err := client.rest.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if _, ok := successCodes[resp.StatusCode]; ok {
		io.Copy(ioutil.Discard, resp.Body)
		return nil
	}

	problem := &Problem{}
	if resp.Header.Get("Content-Type") == problemType {
		if body, err := ioutil.ReadAll(resp.Body); err != nil {
			problem.Status = resp.StatusCode
			problem.Title = http.StatusText(resp.StatusCode)
			problem.Detail = fmt.Sprintf(
				"error reading response body: %v", err)
		} else if err = json.Unmarshal(body, problem); err != nil {
			problem.Status = resp.StatusCode
			problem.Title = http.StatusText(resp.StatusCode)
			problem.Detail = fmt.Sprintf(
				"error unmarshaling response body: %v", err)
		}
		return problem
	}

	io.Copy(ioutil.Discard, resp.Body)
	problem.Status = resp.StatusCode
	problem.Title = http.StatusText(resp.StatusCode)
	return problem
}

// Put sends a PUT request to the REST API of the k8s-crt-dnldr app,
// instructing it to write the certificate file for the specified TLS
// Secret.
//
// If the response has Content-Type application/problem+json, then the
// return value is an instance of *Problem.
func (client *CrtDnldrClient) Put(spec SecretSpec) error {
	return client.mutate(spec, http.MethodPut, putSuccess)
}

// Delete sends a DELETE request to the REST API of the k8s-crt-dnldr
// app, instructing it to delete the certificate file for the
// specified TLS Secret.
//
// If the response has Content-Type application/problem+json, then the
// return value is an instance of *Problem.
func (client *CrtDnldrClient) Delete(spec SecretSpec) error {
	return client.mutate(spec, http.MethodDelete, deleteSuccess)
}
