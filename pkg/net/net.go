/*
 * Copyright (c) 2024 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

// Package net includes common network code used by the Varnish and
// haproxy controllers.
package net

import (
	"crypto/sha256"
	"encoding/binary"
	"net"
	"regexp"
	"strconv"
	"strings"
	"syscall"

	"github.com/sirupsen/logrus"
)

const (
	udsPfx  = "/run/offload/onld_"
	udsSfx  = ".sock"
	encBase = 36
)

var (
	udsAddr      = syscall.RawSockaddrUnix{}
	udsPathLen   = len(udsAddr.Path)
	nonFileChars = regexp.MustCompile(`[^A-Za-z0-9_.]+`)
)

// IsNonTimeoutNetErr returns true for network errors that are not
// timeouts. On syncs for deletion, the Pod may be already gone.
func IsNonTimeoutNetErr(err error, log *logrus.Logger) bool {
	log.Debugf("checking error type %T: %+v", err, err)
	neterr, ok := err.(net.Error)
	if !ok || !neterr.Timeout() {
		return false
	}
	log.Warnf("Non-timeout network error: %+v", err)
	return true
}

func replNonFileChars(s string) string {
	var sb strings.Builder
	sb.WriteRune('_')
	for _, r := range s {
		sb.WriteString(strconv.FormatUint(uint64(r), encBase))
	}
	sb.WriteRune('_')
	return sb.String()
}

func toFileChars(s string) string {
	return nonFileChars.ReplaceAllStringFunc(s, replNonFileChars)
}

// UDSPath returns the path accessed by both Varnish and haproxy for
// the Unix domain socket used for TLS onload to the service
// designated by name.
func UDSPath(name string) string {
	var sb strings.Builder
	sb.WriteString(udsPfx)
	fileStr := toFileChars(name)
	if len(udsPfx)+len(fileStr)+len(udsSfx) < udsPathLen {
		sb.WriteString(fileStr)
	} else {
		hash := sha256.Sum256([]byte(name))
		for i := 0; i < sha256.Size; i += 8 {
			n := binary.NativeEndian.Uint64((hash[i : i+8]))
			sb.WriteString(strconv.FormatUint(n, encBase))
		}
	}
	sb.WriteString(udsSfx)
	return sb.String()
}
