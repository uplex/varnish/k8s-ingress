/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import (
	"fmt"
	"regexp"
	"text/template"
)

const selfShardTmplSrc = `
import std;
import directors;
import blob;
import blobdigest;
import taskvar;
import re2;
import selector;

probe vk8s_probe_varnish {
	.request = "HEAD /vk8s_cluster_health HTTP/1.1"
	           "Host: vk8s_cluster"
	           "Connection: close";
{{- if .Probe }}
{{- if .Probe.Timeout}}
	.timeout = {{.Probe.Timeout}};
{{- end}}
{{- if .Probe.Interval}}
	.interval = {{.Probe.Interval}};
{{- end}}
{{- if .Probe.Initial}}
	.initial = {{.Probe.Initial}};
{{- end}}
{{- if .Probe.Window}}
	.window = {{.Probe.Window}};
{{- end}}
{{- if .Probe.Threshold}}
	.threshold = {{.Probe.Threshold}};
{{- end}}
{{- end}}
}

{{range $node := .Nodes -}}
backend {{nodeName $node}} {
	.host = "{{(index $node.Addresses 0).IP}}";
	.port = "{{(index $node.Addresses 0).Port}}";
	.probe = vk8s_probe_varnish;
}

{{end -}}

acl vk8s_cluster_acl {
{{- range $node := .Nodes}}
	"{{(index $node.Addresses 0).IP}}";
{{- end}}
}

sub vcl_init {
	new vk8s_cluster_param = directors.shard_param();
	new vk8s_cluster = directors.shard();
	vk8s_cluster.associate(vk8s_cluster_param.use());
	{{range $node := .Nodes -}}
	vk8s_cluster.add_backend({{nodeName $node}});
	{{end -}}
	vk8s_cluster.reconfigure();
	new vk8s_cluster_forward = taskvar.bool();
	{{ if hasPrimary . -}}
	new vk8s_cluster_primary = taskvar.backend();
	{{- end }}
{{- range $ridx, $rule := .Rules -}}
	{{- digest_init $rule }}
	{{- if isCookieKey $rule }}
	new vk8s_shard_cookie_{{$ridx}} =
		 re2.regex("\b{{$rule.Key}}\s*=\s*([^,;[:space:]]+)");
	{{- end }}
{{- range $cidx, $c := $rule.Conditions -}}
	{{- if condNeedsMatcher $c }}
	new {{condMatcher $ridx $cidx}} = {{vmod $c.Compare}}.set({{flags $c}});
	{{- range $val := $c.Values}}
	{{condMatcher $ridx $cidx}}.add("{{$val}}");
	{{- end -}}
	{{end -}}
{{- end }}
{{- end }}
}

sub vcl_recv {
	{{ range $ridx, $rule := .Rules -}}
	{{ if $rule.PrimaryOnly -}}
	{{ if $rule.Conditions -}}
	if (
	    {{- range $cidx, $cond := $rule.Conditions}}
	    {{- if ne $cidx 0}} &&
            {{end}}
	    {{- if .Negate}}! {{end}}
	    {{- if condNeedsMatcher $cond}}
	    {{- condMatcher $ridx $cidx}}.{{match .Compare}}({{.Comparand}})
	    {{- else if exists .Compare}}
	    {{- .Comparand}}
	    {{- else}}
            {{- .Comparand}} {{cmpRelation .Compare .Negate}} {{value $cond}}
	    {{- end}}
	    {{- end -}}
	) {
	{{ end -}}
		{{- digest_update 'c' $rule }}
		{{- if isCookieKey $rule }}
		set req.http.VK8S-Shard-Key = "{{ $rule.DefaultKey }}";
		if (vk8s_shard_cookie_{{$ridx}}.match(req.http.Cookie)) {
			set req.http.VK8S-Shard-Key
				= vk8s_shard_cookie_{{$ridx}}.backref(1);
		}
		{{- end }}
		vk8s_cluster_primary.set(vk8s_cluster.backend(resolve=NOW
			{{- key 'c' $rule $ridx }}));
		if (remote.ip !~ vk8s_cluster_acl
		    && "" + vk8s_cluster_primary.get() != server.identity) {
			set req.backend_hint = vk8s_cluster_primary.get();
			return (pipe);
		}
	{{ if $rule.Conditions -}}} else
	{{ end -}}
	{{ end -}}
	{{ end -}}
	if (remote.ip ~ vk8s_cluster_acl) {
		if (req.http.Host == "vk8s_cluster") {
			if (req.url == "/vk8s_cluster_health") {
				return (synth(200));
			}
			return (synth(404));
		}

		# prevent deadlock for accidental cyclic requests
		set req.hash_ignore_busy = true;

		# if we're async, don't deliver stale
		if (req.http.VK8S-Is-Bgfetch == "true") {
			set req.grace = 0s;
		}

		return (hash);
	}
}

sub vk8s_cluster_fetch {
	if (bereq.retries > 0 
	    || bereq.uncacheable
	    || remote.ip ~ vk8s_cluster_acl
	    || "" + vk8s_cluster.backend(resolve=NOW) == server.identity) {
		return;
	}
	{{ range $ridx, $rule := .Rules -}}
	{{ ifElseIf $ridx (len $.Rules) $rule }}
	    {{ if not $rule.PrimaryOnly }}
	        {{- range $cidx, $cond := $rule.Conditions}}
	            {{- if .Negate}}! {{end}}
	            {{- if condNeedsMatcher $cond}}
	            {{- condMatcher $ridx $cidx}}.{{match .Compare}}({{ctx 'b' .Comparand}})
	            {{- else if exists .Compare}}
	            {{- ctx 'b' .Comparand}}
	            {{- else}}
                    {{- ctx 'b' .Comparand}} {{cmpRelation .Compare .Negate}} {{value $cond}}
		    {{- end}}
		{{- end}}
		{{ closeIf $rule }}
		{{- digest_update 'b' $rule }}
		{{- if isCookieKey $rule }}
		set bereq.http.VK8S-Shard-Key = "{{ $rule.DefaultKey }}";
		if (vk8s_shard_cookie_{{$ridx}}.match(bereq.http.Cookie)) {
			set bereq.http.VK8S-Shard-Key
				= vk8s_shard_cookie_{{$ridx}}.backref(1);
		}
		{{- end }}
		vk8s_cluster_param.set({{ key 'b' $rule $ridx }});
		vk8s_cluster_forward.set(true);
		{{ closeRule $rule }}
	    {{- end }}{{/* if not PrimaryOnly */}}
	{{- end }}{{/* range .Rules */}}

	if (vk8s_cluster_forward.get(fallback=false)) {
		set bereq.backend = vk8s_cluster.backend(resolve=LAZY);
		set bereq.http.VK8S-Is-Bgfetch = bereq.is_bgfetch;
		return (fetch);
	}
}

sub vcl_backend_fetch {
	call vk8s_cluster_fetch;
}

sub vcl_backend_response {
	if (bereq.backend == vk8s_cluster.backend(resolve=LAZY)) {
	        if (beresp.http.VK8S-Cluster-TTL) {
		        set beresp.ttl = std.duration(
                	    beresp.http.VK8S-Cluster-TTL + "s", 1s);
		        if (beresp.ttl > {{.MaxSecondaryTTL}}) {
			        set beresp.ttl = {{.MaxSecondaryTTL}};
		        }
		        unset beresp.http.VK8S-Cluster-TTL;
	        }
                else {
			set beresp.uncacheable = true;
		}
		return (deliver);
	}
}

sub vcl_backend_error {
	if (bereq.backend == vk8s_cluster.backend(resolve=LAZY)) {
		return (deliver);
	}
}

sub vcl_deliver {
	unset resp.http.VK8S-Cluster-TTL;
	if (remote.ip ~ vk8s_cluster_acl
		{{- if hasPrimary . }} && ! vk8s_cluster_primary.defined()
		{{- end }}) {
		if (! obj.uncacheable) {
			set resp.http.VK8S-Cluster-TTL = obj.ttl;
		}
		return (deliver);
	}
}
`

var reqMatch = regexp.MustCompile("^req")

func context(ctx rune, key string) string {
	if ctx == 'c' {
		return key
	}
	return reqMatch.ReplaceAllLiteralString(key, "bereq")
}

func keyParams(ctx rune, rule ShardRule, idx int) string {
	pfx := ""
	http := "bereq"
	if ctx == 'c' {
		pfx = ", "
		http = "req"
	}
	switch rule.By {
	case ByHash:
		return ""
	case URL:
		return pfx + "by=URL"
	case Key:
		return pfx + "by=KEY, key=vk8s_cluster.key(" +
			context(ctx, rule.Key) + ")"
	case Cookie:
		return pfx + "by=KEY, key=vk8s_cluster.key(" + http +
			".http.VK8S-Shard-Key)"
	}
	return pfx + "by=BLOB, key_blob=vk8s_shard_digest.final()"
}

func isCookieKey(rule ShardRule) bool {
	return rule.By == Cookie
}

func digestInit(rule ShardRule) string {
	if rule.By != Blob {
		return ""
	}
	return "\n\tnew vk8s_shard_digest = blobdigest.digest(" +
		rule.Algo.String() + ");"
}

func digestUpdate(ctx rune, rule ShardRule) string {
	if rule.By != Blob {
		return ""
	}
	return "\n\tvk8s_shard_digest.update(blob.decode(encoded=" +
		context(ctx, rule.Key) + "));"
}

func hasPrimary(shard ShardCluster) bool {
	for _, rule := range shard.Rules {
		if rule.PrimaryOnly {
			return true
		}
	}
	return false
}

func ifElseIf(ridx, rlen int, rule ShardRule) string {
	if rule.PrimaryOnly || len(rule.Conditions) == 0 {
		return ""
	}
	if ridx == 1 {
		return "if"
	}
	if ridx == rlen-1 {
		return "else"
	}
	return "elsif"
}

func closeIf(rule ShardRule) string {
	if len(rule.Conditions) == 0 {
		return ""
	}
	return ") {"
}

func closeRule(rule ShardRule) string {
	if len(rule.Conditions) == 0 {
		return ""
	}
	return "}"
}

const selfShardName = "self-sharding"

var shardFuncMap = template.FuncMap{
	"key":           keyParams,
	"isCookieKey":   isCookieKey,
	"digest_init":   digestInit,
	"digest_update": digestUpdate,
	"hasPrimary":    hasPrimary,
	"ifElseIf":      ifElseIf,
	"closeIf":       closeIf,
	"closeRule":     closeRule,
	"nodeName": func(svc Service) string {
		// MUST match -i setting in the varnishd invocaton
		// (server.identity)
		addr := svc.Addresses[0]
		return "vk8s_" + addr.PodNamespace + "_" + addr.PodName
	},
	"exists": func(cmp CompareType) bool { return cmp == Exists },
	"match":  func(cmp CompareType) string { return match(cmp) },
	"value":  func(cond Condition) string { return reqValue(cond) },
	"vmod":   func(cmp CompareType) string { return vmod(cmp) },
	"flags":  func(cond Condition) string { return reqFlags(cond) },
	"cmpRelation": func(cmp CompareType, negate bool) string {
		return cmpRelation(cmp, negate)
	},
	"condNeedsMatcher": func(cond Condition) bool {
		return reqNeedsMatcher(cond)
	},
	"condMatcher": func(ridx, cidx int) string {
		return fmt.Sprintf("vk8s_selfshard_cond_%d_%d", ridx, cidx)
	},
	"ctx": context,
}

var shardTmpl = template.Must(template.New(selfShardName).Funcs(shardFuncMap).
	Parse(selfShardTmplSrc))

// ResetShardTmpl sets the VCL template for the VarnishConfig
// shard feature to its current "official" value. Invoked on
// TemplateConfig deletion, only needed when devmode is activated for
// the controller.
func ResetShardTmpl() {
	shardTmpl = template.Must(template.New(selfShardName).
		Funcs(shardFuncMap).Parse(selfShardTmplSrc))
}

// SetShardTmpl parses src as a text/template, using the FuncMap
// defined for the shard template, which is used to generate VCL for
// the VarnishConfig shard feature. On success, the shard template
// is replaced. If the parse fails, then the error is returned and the
// shard template is unchanged.
//
// Only used when devmode is activated for the controller.
func SetShardTmpl(src string) error {
	newTmpl, err := template.New(selfShardName).Funcs(shardFuncMap).
		Parse(src)
	if err != nil {
		return err
	}
	shardTmpl = newTmpl
	return nil
}
