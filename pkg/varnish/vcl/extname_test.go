/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import "testing"

var extWhiskeySvc = Service{
	Name:               "whiskey-svc",
	ExternalName:       "whiskey.example.com",
	ExternalPort:       "81",
	DNSRetryDelay:      "30s",
	FollowDNSRedirects: true,
}

var extVodkaSvc = Service{
	Name:               "vodka-svc",
	ExternalName:       "vodka.example.com",
	ExternalPort:       "8888",
	DNSRetryDelay:      "30s",
	FollowDNSRedirects: true,
}

var barSpec = Spec{
	DefaultService: Service{},
	Rules: []Rule{
		{
			Host: "cafe.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/tea",
					Type: PathPrefix,
				}: teaSvc,
				{
					Path: "/coffee",
					Type: PathPrefix,
				}: coffeeSvc,
			},
		},
		{
			Host: "whiskey.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: extWhiskeySvc,
			},
		},
		{
			Host: "vodka.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: extVodkaSvc,
			},
		},
	},
	IntSvcs: map[string]Service{
		"tea-svc":    teaSvc,
		"coffee-svc": coffeeSvc,
	},
	ExtSvcs: map[string]Service{
		"whiskey-svc": extWhiskeySvc,
		"vodka-svc":   extVodkaSvc,
	},
}

func TestExternalNameSvc(t *testing.T) {
	templateTest(t, ingressTmpl, barSpec, "extname.golden")
}

var extTequilaSvc = Service{
	Name:         "tequila-svc",
	ExternalName: "tequila.example.com",
	ExternalPort: "88",
	NameTTL: &NameTTLSpec{
		TTL:  "2h",
		From: FromCfg,
	},
	DNSRetryDelay:      "30s",
	FollowDNSRedirects: true,
	Probe: &Probe{
		URL:         "/shot/",
		ExpResponse: 418,
		Timeout:     "5s",
		Interval:    "10s",
		Initial:     "1",
		Window:      "4",
		Threshold:   "3",
	},
	HostHeader:          "tequila.example.org",
	ConnectTimeout:      "1s",
	FirstByteTimeout:    "2s",
	BetweenBytesTimeout: "3s",
	MaxConnections:      100,
	ProxyHeader:         2,
}

var extMetaxaSvc = Service{
	Name:         "metaxa-svc",
	ExternalName: "metaxa.example.com",
	ExternalPort: "8080",
	NameTTL: &NameTTLSpec{
		From: FromMax,
	},
	DNSRetryDelay:      "30s",
	FollowDNSRedirects: true,
	Probe: &Probe{
		Request: []string{
			"GET /the-worm/ HTTP/1.1",
			"Host: mextaxa.example.org",
			"Connection: close",
		},
		Timeout:   "10s",
		Interval:  "20s",
		Initial:   "2",
		Window:    "5",
		Threshold: "3",
	},
	HostHeader:          "metaxa.example.org",
	ConnectTimeout:      "2s",
	FirstByteTimeout:    "3s",
	BetweenBytesTimeout: "4s",
	MaxConnections:      200,
	ProxyHeader:         1,
}

var boozeSpec = Spec{
	DefaultService: Service{},
	Rules: []Rule{
		{
			Host: "tequila.example.edu",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: extTequilaSvc,
			},
		},
		{
			Host: "metaxa.example.edu",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: extMetaxaSvc,
			},
		},
	},
	ExtSvcs: map[string]Service{
		"tequila-svc": extTequilaSvc,
		"metaxa-svc":  extMetaxaSvc,
	},
}

func TestExternalNameBcfg(t *testing.T) {
	templateTest(t, ingressTmpl, boozeSpec, "extname_bcfg.golden")
}
