/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import "testing"

var cafeNoHostSpec = Spec{
	Rules: []Rule{
		{
			Host: "tea.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: teaSvc,
			},
		},
		{
			PathMap: map[PathKey]Service{
				{
					Path: "/tea",
					Type: PathPrefix,
				}: teaSvc,
				{
					Path: "/coffee",
					Type: PathPrefix,
				}: coffeeSvc,
			},
		},
	},
	IntSvcs: map[string]Service{
		"tea-svc":    teaSvc,
		"coffee-svc": coffeeSvc,
	},
}

func TestIngressFanoutNoHost(t *testing.T) {
	templateTest(t, ingressTmpl, cafeNoHostSpec,
		"ingress_fanout_nohost.golden")
}

var h2oSvc = Service{
	Name: "water-svc",
	Addresses: []Address{
		{
			IP:   "192.0.2.128",
			Port: 8000,
		},
		{
			IP:   "192.0.2.129",
			Port: 8000,
		},
		{
			IP:   "192.0.2.130",
			Port: 8000,
		},
	},
}

var cafeVhostNoHostSpec = Spec{
	Rules: []Rule{
		{
			Host: "tea.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: teaSvc,
			},
		},
		{
			Host: "coffee.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: coffeeSvc,
			},
		},
		{
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: h2oSvc,
			},
		},
	},
	IntSvcs: map[string]Service{
		"tea-svc":    teaSvc,
		"coffee-svc": coffeeSvc,
		"water-svc":  h2oSvc,
	},
}

func TestIngressVhostNoHost(t *testing.T) {
	templateTest(t, ingressTmpl, cafeVhostNoHostSpec,
		"ingress_vhost_nohost.golden")
}

var singleSvcSpec = Spec{
	DefaultService: teaSvc,
	IntSvcs:        map[string]Service{"tea-svc": teaSvc},
}

func TestIngressSingleSvc(t *testing.T) {
	templateTest(t, ingressTmpl, singleSvcSpec, "ingress_single_svc.golden")
}

var defaultSvcProbeDir = Spec{
	DefaultService: teaSvcProbeDir,
	IntSvcs:        map[string]Service{"tea-svc": teaSvcProbeDir},
}

func TestIngressDefaultProbeDir(t *testing.T) {
	templateTest(t, ingressTmpl, defaultSvcProbeDir,
		"ingress_default_probe_dir.golden")
}

var barDefaultProbeDir = Spec{
	DefaultService: extTequilaSvc,
	Rules:          append(cafeProbeDir.Rules, boozeSpec.Rules...),
	IntSvcs:        cafeProbeDir.IntSvcs,
	ExtSvcs:        boozeSpec.ExtSvcs,
}

func TestIngressDefaultRules(t *testing.T) {
	templateTest(t, ingressTmpl, barDefaultProbeDir,
		"ingress_default_rules.golden")
}

var cafeBarPathsSpec = Spec{
	Rules: []Rule{
		{
			PathMap: map[PathKey]Service{
				{
					Path: "/tea",
					Type: PathExact,
				}: teaSvc,
				{
					Path: "/coffee",
					Type: PathExact,
				}: coffeeSvc,
				{
					Path: "/whiskey",
					Type: PathPrefix,
				}: extWhiskeySvc,
				{
					Path: "/vodka",
					Type: PathPrefix,
				}: extVodkaSvc,
				{
					Path: "/milk",
					Type: PathImplSpecific,
				}: milkSvcProbeDir,
				{
					Path: "/water",
					Type: PathImplSpecific,
				}: h2oSvc,
			},
		},
	},
	IntSvcs: map[string]Service{
		"tea-svc":     teaSvc,
		"coffee-svc":  coffeeSvc,
		"whiskey-svc": extWhiskeySvc,
		"vodka-svc":   extVodkaSvc,
		"milk-svc":    milkSvcProbeDir,
		"h2o-svc":     h2oSvc,
	},
}

func TestIngressPathTypes(t *testing.T) {
	templateTest(t, ingressTmpl, cafeBarPathsSpec,
		"ingress_path_types.golden")
}

var wildcardSpec = Spec{
	Rules: []Rule{
		{
			Host: "*.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: coffeeSvc,
			},
		},
	},
	IntSvcs: map[string]Service{
		"coffee-svc": coffeeSvc,
	},
}

func TestWildcardHost(t *testing.T) {
	templateTest(t, ingressTmpl, wildcardSpec, "wildcard.golden")
}
