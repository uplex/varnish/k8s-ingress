/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import "text/template"

const authTmplSrc = `
import re2;

sub vcl_init {
{{- range $auth := .Auths}}
	new {{credsMatcher .Realm}} = re2.set(anchor=both);
	{{- range $cred := .Credentials}}
	{{credsMatcher $auth.Realm}}.add("Basic\s+\Q{{$cred}}\E\s*");
	{{- end}}
	{{credsMatcher .Realm}}.compile();
{{end -}}
}

sub vcl_recv {
{{- range .Auths}}
	if (
	    {{- range .Conditions}}
	    {{.Comparand}} {{cmpRelation .Compare .Negate}} "{{.Value}}" &&
	    {{- end}}
	    {{- if eq .Status 401}}
	    !{{credsMatcher .Realm}}.match(req.http.Authorization)
	    {{- else}}
	    !{{credsMatcher .Realm}}.match(req.http.Proxy-Authorization)
	    {{- end}}
	   ) {
		{{- if .UTF8 }}
		set req.http.VK8S-Authenticate =
			{"Basic realm="{{.Realm}}", charset="UTF-8""};
		{{- else}}
		set req.http.VK8S-Authenticate = {"Basic realm="{{.Realm}}""};
		{{- end}}
		return(synth(60000 + {{.Status}}));
	}
{{- end}}
}

sub vcl_synth {
	if (resp.status == 60401) {
		set resp.http.WWW-Authenticate = req.http.VK8S-Authenticate;
		return(deliver);
	}
	if (resp.status == 60407) {
		set resp.http.Proxy-Authenticate = req.http.VK8S-Authenticate;
		return(deliver);
	}
}
`

const authTmplName = "auth"

var authFuncs = template.FuncMap{
	"credsMatcher": func(realm string) string {
		return mangle(realm + "_auth")
	},
	"cmpRelation": func(cmp CompareType, negate bool) string {
		return cmpRelation(cmp, negate)
	},
}

var authTmpl = template.Must(template.New(authTmplName).Funcs(authFuncs).
	Parse(authTmplSrc))

// ResetAuthTmpl sets the VCL template for the VarnishConfig auth
// feature to its current "official" value. Invoked on TemplateConfig
// deletion, only needed when devmode is activated for the controller.
func ResetAuthTmpl() {
	authTmpl = template.Must(template.New(authTmplName).Funcs(authFuncs).
		Parse(authTmplSrc))
}

// SetAuthTmpl parses src as a text/template, using the FuncMap
// defined for the auth template, which is used to generate VCL for
// the VarnishConfig auth feature. On success, the auth template is
// replaced. If the parse fails, then the error is returned and the
// auth template is unchanged.
//
// Only used when devmode is activated for the controller.
func SetAuthTmpl(src string) error {
	newTmpl, err := template.New(authTmplName).Funcs(authFuncs).Parse(src)
	if err != nil {
		return err
	}
	authTmpl = newTmpl
	return nil
}
