/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import (
	"fmt"
	"strings"
	"text/template"
)

const aclTmplSrc = `
import std;

{{- range .ACLs}}
acl {{aclName .Name}} {
	{{- range .Addresses}}
	{{if .Negate}}! {{end}}"{{.Addr}}"{{aclMask .MaskBits}};
	{{- end}}
}
{{- end}}

sub vcl_recv {
	{{- if hasXFF .ACLs}}
        std.collect(req.http.X-Forwarded-For);
        {{- end}}
	{{- range .ACLs}}
	if (
	    {{- range $cond := .Conditions}}
	    {{$cond.Comparand}} {{cmpRelation .Compare .Negate}} "{{.Value}}" &&
	    {{- end}}
	    {{aclCmp .Comparand}} {{if .Whitelist}}!{{end}}~ {{aclName .Name}}
	   ) {
		{{- if .ResultHdr.Header}}
		set {{.ResultHdr.Header}} = "{{.ResultHdr.Failure}}";
		{{- end}}
		{{- if ge .FailStatus 100}}
		return(synth({{.FailStatus}}));
		{{- end}}
	}
	{{- if .ResultHdr.Header}}
	else {
		set {{.ResultHdr.Header}} = "{{.ResultHdr.Success}}";
	}
	{{- end}}
	{{- end}}
}
`

const aclTmplName = "acl"

const (
	xffFirst   = `regsub(req.http.X-Forwarded-For,"^([^,\s]+).*","\1")`
	xff2ndLast = `regsub(req.http.X-Forwarded-For,"^.*?([[:xdigit:]:.]+)\s*,[^,]*$","\1")`
)

func hasXFF(acls []ACL) bool {
	for _, acl := range acls {
		if strings.HasPrefix(acl.Comparand, "xff-") {
			return true
		}
	}
	return false
}

func aclCmp(comparand string) string {
	if strings.HasPrefix(comparand, "xff-") ||
		strings.HasPrefix(comparand, "req.http.") {

		if comparand == "xff-first" {
			comparand = xffFirst
		} else if comparand == "xff-2ndlast" {
			comparand = xff2ndLast
		}
		return fmt.Sprintf(`std.ip(%s, "0.0.0.0")`, comparand)
	}
	return comparand
}

func aclMask(bits uint8) string {
	if bits > 128 {
		return ""
	}
	return fmt.Sprintf("/%d", bits)
}

var aclFuncs = template.FuncMap{
	"aclMask": func(bits uint8) string { return aclMask(bits) },
	"hasXFF":  func(acls []ACL) bool { return hasXFF(acls) },
	"aclName": func(name string) string {
		return mangle(name + "_acl")
	},
	"cmpRelation": func(cmp CompareType, negate bool) string {
		return cmpRelation(cmp, negate)
	},
	"aclCmp": func(comparand string) string {
		return aclCmp(comparand)
	},
}

var aclTmpl = template.Must(template.New(aclTmplName).Funcs(aclFuncs).
	Parse(aclTmplSrc))

// ResetACLTmpl sets the VCL template for the VarnishConfig acl
// feature to its current "official" value. Invoked on TemplateConfig
// deletion, only needed when devmode is activated for the controller.
func ResetACLTmpl() {
	aclTmpl = template.Must(template.New(aclTmplName).Funcs(aclFuncs).
		Parse(aclTmplSrc))
}

// SetACLTmpl parses src as a text/template, using the FuncMap defined
// for the ACL template, which is used to generate VCL for the
// VarnishConfig acl feature. On success, the ACL template is
// replaced. If the parse fails, then the error is returned and the
// ACL template is unchanged.
//
// Only used when devmode is activated for the controller.
func SetACLTmpl(src string) error {
	newTmpl, err := template.New(aclTmplName).Funcs(aclFuncs).Parse(src)
	if err != nil {
		return err
	}
	aclTmpl = newTmpl
	return nil
}
