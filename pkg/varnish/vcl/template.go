/*
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import (
	"bytes"
	"fmt"
	"hash/fnv"
	"math/big"
	"regexp"
	"strconv"
	"strings"
)

// GetSrc returns the VCL generated to implement a Spec.
func (spec Spec) GetSrc() (string, error) {
	var buf bytes.Buffer
	if err := ingressTmpl.Execute(&buf, spec); err != nil {
		return "", err
	}
	if len(spec.ShardCluster.Nodes) > 0 {
		if err := shardTmpl.Execute(&buf, spec.ShardCluster); err != nil {
			return "", err
		}
	}
	if len(spec.ACLs) > 0 {
		if err := aclTmpl.Execute(&buf, spec); err != nil {
			return "", err
		}
	}
	if len(spec.Auths) > 0 {
		if err := authTmpl.Execute(&buf, spec); err != nil {
			return "", err
		}
	}
	if len(spec.Rewrites) > 0 {
		if err := rewriteTmpl.Execute(&buf, spec); err != nil {
			return "", err
		}
	}
	if len(spec.Dispositions) > 0 {
		if err := reqDispTmpl.Execute(&buf, spec); err != nil {
			return "", err
		}
	}
	if spec.VCL != "" {
		buf.WriteString(spec.VCL)
	}
	return buf.String(), nil
}

// The following functions are re-used in the function maps of more
// than one template.

var vclIllegal = regexp.MustCompile("[^[:word:]-]+")

func replIllegal(ill []byte) []byte {
	repl := []byte("_")
	for _, b := range ill {
		repl = append(repl, []byte(fmt.Sprintf("%02x", b))...)
	}
	repl = append(repl, []byte("_")...)
	return repl
}

func bound(s string, l int) string {
	if len(s) <= l {
		return s
	}
	b := []byte(s)
	h := fnv.New32a()
	h.Write(b)
	i := big.NewInt(int64(h.Sum32()))
	b[l-7] = byte('_')
	copy(b[l-6:l], []byte(i.Text(62)))
	return string(b[0:l])
}

func mangle(s string) string {
	if s == "" {
		return s
	}
	prefixed := "vk8s_" + s
	bytes := []byte(prefixed)
	return string(vclIllegal.ReplaceAllFunc(bytes, replIllegal))
}

func backendName(svc Service, addr Address) string {
	n := ""
	if addr.PodNamespace != "" && addr.PodName != "" {
		n = addr.PodNamespace + "_" + addr.PodName
	} else {
		n = svc.Name + "_" + strings.Replace(addr.IP, ".", "_", -1)
	}
	return mangle(n + "_" + strconv.Itoa(int(addr.Port)))
}

func cmpRelation(cmp CompareType, negate bool) string {
	switch cmp {
	case Equal:
		if negate {
			return "!="
		}
		return "=="
	case Match:
		if negate {
			return "!~"
		}
		return "~"
	case Greater:
		return ">"
	case GreaterEqual:
		return ">="
	case Less:
		return "<"
	case LessEqual:
		return "<="
	default:
		return "__INVALID_COMPARISON_TYPE__"
	}
}

func vmod(cmp CompareType) string {
	switch cmp {
	case Equal, Prefix:
		return "selector"
	case Match:
		return "re2"
	default:
		return "__INVALID_COMPARISON_FOR_VMOD__"
	}
}

func matcherFlags(isSelector bool, flagSpec MatchFlagsType) string {
	if isSelector {
		if !flagSpec.CaseSensitive {
			return "case_sensitive=false"
		}
		return ""
	}

	var flags []string
	if flagSpec.MaxMem != 0 && flagSpec.MaxMem != 8388608 {
		maxMem := fmt.Sprintf("max_mem=%d", flagSpec.MaxMem)
		flags = append(flags, maxMem)
	}
	if flagSpec.Anchor != None {
		switch flagSpec.Anchor {
		case Start:
			flags = append(flags, "anchor=start")
		case Both:
			flags = append(flags, "anchor=both")
		}
	}
	if flagSpec.UTF8 {
		flags = append(flags, "utf8=true")
	}
	if flagSpec.PosixSyntax {
		flags = append(flags, "posix_syntax=true")
	}
	if flagSpec.LongestMatch {
		flags = append(flags, "longest_match=true")
	}
	if flagSpec.Literal {
		flags = append(flags, "literal=true")
	}
	if !flagSpec.CaseSensitive {
		flags = append(flags, "case_sensitive=false")
	}
	if flagSpec.PerlClasses {
		flags = append(flags, "perl_classes=true")
	}
	if flagSpec.WordBoundary {
		flags = append(flags, "word_boundary=true")
	}
	return strings.Join(flags, ",")
}

func match(cmp CompareType) string {
	switch cmp {
	case Match, Equal:
		return "match"
	case Prefix:
		return "hasprefix"
	default:
		return "__INVALID_MATCH_OPERATION__"
	}
}
