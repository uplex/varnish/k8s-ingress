/*-
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

// Package update defines the Status type, which classifies the result
// of a synchronization operation by the controller. This
// distinguishes forms of success or failure after an
// Add/Update/Delete notification for the resource types that the
// controller watches, and determeins furher actions such as error
// handling, logging and event generation.
package update

import "fmt"

// StatusType is the classifier for the result of a synchronization.
type StatusType uint8

const (
	// Noop indicates that no change in the cluster was
	// necessary. The result is logged, but no further action is
	// taken.
	Noop StatusType = iota
	// Success indicates that a change in the cluster was
	// successfully executed. The result is logged, and an event
	// is generated for the resource that was synchronized.
	Success
	// Fatal indicates an unrecoverable error, which cannot be
	// compensated by retrying the same synchronization. For
	// example, an invalid configuration is unrecoverable, if it
	// can never become valid until it is changed. This result is
	// logged, a warning event is generated, and the operation is
	// never retried.
	Fatal
	// Recoverable indicates an error that might be compensated
	// with a retry. For example, network errors in the cluster
	// such as a timeouts may be temporary, and hence may succeed
	// on retry.  This result is logged, a warning event is
	// generated, and the sync operation is re-queued with a rate
	// limiting delay (likely an exponential backoff, as scheduled
	// by the client-go workqueue).
	Recoverable
	// Incomplete indicates that a cluster change is necessary,
	// but the controller does not currently have all of the
	// information that it requires. For example, a Service
	// configuration may have been added, but the Endpoints are
	// not yet known (the Service's Pods might still be starting
	// up). This result is logged, a warning event is generated,
	// and the sync operation is scheduled for retry after a brief
	// delay (default 5s), since the missing info commonly becomes
	// available within a brief time.
	Incomplete
)

func (t StatusType) String() string {
	switch t {
	case Noop:
		return "no cluster change necessary"
	case Success:
		return "successfully synced"
	case Fatal:
		return "unrecoverable error"
	case Recoverable:
		return "recoverable error"
	case Incomplete:
		return "insufficient info for necessary cluster change"
	default:
		return "UNKNOWN SYNC STATUS"
	}
}

// Reason returns a string suitable as the reason string for an event
// after a sync returns the given status type.
//
//	Success:     SyncSuccess
//	Fatal:       SyncFatalError
//	Recoverable: SyncRecoverableError
//	Incomplete:  SyncIncomplete
//
// Not valid for the Noop type, since no event is generated for Noop.
func (t StatusType) Reason() string {
	switch t {
	case Success:
		return "SyncSuccess"
	case Fatal:
		return "SyncFatalError"
	case Recoverable:
		return "SyncRecoverableError"
	case Incomplete:
		return "SyncIncomplete"
	default:
		panic("illegal StatusType for Reason()")
	}
}

// Status encapsulates the result of a synchronization operation,
// including its type and an optional detail message (an error message
// for the error types).
type Status struct {
	Msg  string
	Type StatusType
	Obj  interface{}
}

func (status Status) String() string {
	if status.Msg == "" {
		return status.Type.String()
	}
	return status.Type.String() + ": " + status.Msg
}

// Reason returns a string suitable as the reason string for an event
// after a sync returns. Identical to Reason() for the given type.
func (status Status) Reason() string {
	return status.Type.Reason()
}

// IsError returns true iff the Status has one of the error types:
// Fatal, Recoverable or Incomplete.
func (status Status) IsError() bool {
	switch status.Type {
	case Noop:
		return false
	case Success:
		return false
	case Fatal:
		return true
	case Recoverable:
		return true
	case Incomplete:
		return true
	default:
		return true
	}
}

func (status Status) Error() string {
	return status.String()
}

// Make is a convenience function to create a Status of the given
// type, setting the Msg to a formatted string.
func Make(t StatusType, format string, args ...interface{}) Status {
	return Status{
		Msg:  fmt.Sprintf(format, args...),
		Type: t,
	}
}

// MakeNoop creates a Noop Status with a formatted string for the Msg.
func MakeNoop(format string, args ...interface{}) Status {
	return Make(Noop, format, args...)
}

// MakeSuccess creates a Success Status with a formatted string for
// the Msg.
func MakeSuccess(format string, args ...interface{}) Status {
	return Make(Success, format, args...)
}

// MakeFatal creates a Fatal Status with a formatted string for the
// Msg.
func MakeFatal(format string, args ...interface{}) Status {
	return Make(Fatal, format, args...)
}

// MakeRecoverable creates a Recoverable Status with a formatted
// string for the Msg.
func MakeRecoverable(format string, args ...interface{}) Status {
	return Make(Recoverable, format, args...)
}

// MakeIncomplete creates an Incomplete Status with a formatted string
// for the Msg.
func MakeIncomplete(format string, args ...interface{}) Status {
	return Make(Incomplete, format, args...)
}

// HasOnlyErrors returns true if all of the Statuses in the slice are
// errors (Fatal, Recoverable or Incomplete).
func HasOnlyErrors(statuses []Status) bool {
	for _, s := range statuses {
		if !s.IsError() {
			return false
		}
	}
	return true
}
