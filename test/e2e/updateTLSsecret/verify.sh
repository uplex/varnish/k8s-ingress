#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../utils.sh

LOCALPORT=${LOCALPORT:-4443}

# Initially the same test as in examples/tls/hello

wait_until_ready app=varnish-ingress

wait_until_configured app=varnish-ingress default 600

kubectl port-forward svc/varnish-ingress ${LOCALPORT}:443 >/dev/null &
ret=$?
if [ $ret -ne 0 ]; then
    exit $ret
fi
trap 'kill $(jobs -p)' EXIT

wait_for_port ${LOCALPORT}
set +e

sleep 1
varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
ret=$?
if [ $ret -eq 77 ]; then
    exit 0
elif [ $ret -ne 0 ]; then
    exit $ret
fi

# Verify the TLS cert

CONNECT=cafe.example.com:443:localhost:4443
URI=https://cafe.example.com/coffee/foo/bar
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'issuer:.+CN=cafe.example.com'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'issuer:.+O=Green Midget Cafe'

# Change the TLS key
kubectl create secret tls cafe-tls-secret --save-config --dry-run=client --key=./clinic.key --cert=./clinic.crt -o yaml | kubectl apply -f -

# Wait for the Secret and Ingress config to update
set +e
N=0
while true; do
    sleep 1
    kubectl get event --sort-by=.lastTimestamp --field-selector involvedObject.name=cafe-tls-secret | tail -1 | grep -q -E 'SyncSuccess.+update default/cafe-tls-secret.+requeued Ingress'
    if [ $? -eq 0 ]; then
        break
    fi
    if [ $N -ge 120 ]; then
        echo "Timed out waiting for Secret to update"
        exit 1
    fi
    N=$(( N + 1))
done

sleep 10

N=0
while true; do
    sleep 1
    kubectl get event --sort-by=.lastTimestamp --field-selector involvedObject.name=cafe-ingress | tail -1 | grep -q -E 'SyncSuccess.+update default/cafe-ingress'
    if [ $? -eq 0 ]; then
        break
    fi
    if [ $N -ge 120 ]; then
        echo "Timed out waiting for Ingress to update"
        exit 1
    fi
    N=$(( N + 1))
done
set -e

# Test again
sleep 1
varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
ret=$?
if [ $ret -eq 77 ]; then
    exit 0
elif [ $ret -ne 0 ]; then
    exit $ret
fi

# Verify that the new TLS cert is being used

CONNECT=cafe.example.com:443:localhost:4443
URI=https://cafe.example.com/coffee/foo/bar
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'issuer:.+CN=cafe.example.com'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'issuer:.+O=Argument Clinic'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'issuer:.+OU=Abuse'

exit 0
