#! /bin/bash -ex

if [ $# -ne 1 ]; then
    echo "Usage: $0 file.vtc"
    exit 1
fi

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../utils.sh

LOCALPORT=${LOCALPORT:-8888}

wait_until_ready app=varnish-ingress
wait_until_configured app=varnish-ingress

kubectl port-forward svc/varnish-ingress ${LOCALPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} $1
