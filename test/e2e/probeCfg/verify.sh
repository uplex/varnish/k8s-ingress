#! /bin/bash -ex

if [ $# -ne 1 ]; then
    echo "Usage: $0 file.vtc"
    exit 1
fi

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../utils.sh

LOCALPORT=${LOCALPORT:-8888}

wait_until_ready app.kubernetes.io/name=viking-controller probe-cfg
wait_until_ready app.kubernetes.io/name=viking-service probe-cfg
wait_until_configured app.kubernetes.io/name=viking-service probe-cfg

# Verify the probe configuration properties set in values-viking.yaml
JSONPATH_VARNISH='{.spec.containers[?(@.name=="varnish")]'
JSONPATH_VARNISH_LIVE=${JSONPATH_VARNISH}.livenessProbe
JSONPATH_VARNISH_READY=${JSONPATH_VARNISH}.readinessProbe

JSONPATH_HAPROXY='{.spec.containers[?(@.name=="haproxy")]'
JSONPATH_HAPROXY_LIVE=${JSONPATH_HAPROXY}.livenessProbe
JSONPATH_HAPROXY_READY=${JSONPATH_HAPROXY}.readinessProbe

PODS=$(kubectl -n probe-cfg get pod -l app.kubernetes.io/name=viking-service -o=name)
for pod in ${PODS}; do
    # Varnish liveness probe
    DELAY=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_LIVE}.initialDelaySeconds}")
    if [ ${DELAY} -ne "1" ]; then
        exit 1
    fi
    PERIOD=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_LIVE}.periodSeconds}")
    if [ ${PERIOD} -ne "5" ]; then
        exit 1
    fi
    TIMEOUT=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_LIVE}.timeoutSeconds}")
    if [ ${TIMEOUT} -ne "2" ]; then
        exit 1
    fi
    SUCCESS=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_LIVE}.successThreshold}")
    if [ ${SUCCESS} -ne "1" ]; then
        exit 1
    fi
    FAILURE=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_LIVE}.failureThreshold}")
    if [ ${FAILURE} -ne "2" ]; then
        exit 1
    fi

    # Varnish readiness probe
    DELAY=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_READY}.initialDelaySeconds}")
    if [ ${DELAY} -ne "5" ]; then
        exit 1
    fi
    PERIOD=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_READY}.periodSeconds}")
    if [ ${PERIOD} -ne "10" ]; then
        exit 1
    fi
    TIMEOUT=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_READY}.timeoutSeconds}")
    if [ ${TIMEOUT} -ne "9" ]; then
        exit 1
    fi
    SUCCESS=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_READY}.successThreshold}")
    if [ ${SUCCESS} -ne "2" ]; then
        exit 1
    fi
    FAILURE=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_VARNISH_READY}.failureThreshold}")
    if [ ${FAILURE} -ne "3" ]; then
        exit 1
    fi

    # haproxy liveness probe
    DELAY=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_LIVE}.initialDelaySeconds}")
    if [ ${DELAY} -ne "3" ]; then
        exit 1
    fi
    PERIOD=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_LIVE}.periodSeconds}")
    if [ ${PERIOD} -ne "3" ]; then
        exit 1
    fi
    TIMEOUT=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_LIVE}.timeoutSeconds}")
    if [ ${TIMEOUT} -ne "2" ]; then
        exit 1
    fi
    SUCCESS=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_LIVE}.successThreshold}")
    if [ ${SUCCESS} -ne "1" ]; then
        exit 1
    fi
    FAILURE=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_LIVE}.failureThreshold}")
    if [ ${FAILURE} -ne "3" ]; then
        exit 1
    fi

    # haproxy readiness probe
    DELAY=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_READY}.initialDelaySeconds}")
    if [ ${DELAY} -ne "4" ]; then
        exit 1
    fi
    PERIOD=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_READY}.periodSeconds}")
    if [ ${PERIOD} -ne "5" ]; then
        exit 1
    fi
    TIMEOUT=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_READY}.timeoutSeconds}")
    if [ ${TIMEOUT} -ne "4" ]; then
        exit 1
    fi
    SUCCESS=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_READY}.successThreshold}")
    if [ ${SUCCESS} -ne "3" ]; then
        exit 1
    fi
    FAILURE=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_HAPROXY_READY}.failureThreshold}")
    if [ ${FAILURE} -ne "2" ]; then
        exit 1
    fi
done

JSONPATH_CONTROLLER='{.spec.containers[?(@.name=="controller")]'
JSONPATH_CONTROLLER_LIVE=${JSONPATH_CONTROLLER}.livenessProbe
JSONPATH_CONTROLLER_READY=${JSONPATH_CONTROLLER}.readinessProbe

PODS=$(kubectl -n probe-cfg get pod -l app.kubernetes.io/name=viking-controller -o=name)
for pod in ${PODS}; do
    # Controller liveness probe
    DELAY=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_LIVE}.initialDelaySeconds}")
    if [ ${DELAY} -ne "1" ]; then
        exit 1
    fi
    PERIOD=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_LIVE}.periodSeconds}")
    if [ ${PERIOD} -ne "5" ]; then
        exit 1
    fi
    TIMEOUT=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_LIVE}.timeoutSeconds}")
    if [ ${TIMEOUT} -ne "2" ]; then
        exit 1
    fi
    SUCCESS=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_LIVE}.successThreshold}")
    if [ ${SUCCESS} -ne "1" ]; then
        exit 1
    fi
    FAILURE=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_LIVE}.failureThreshold}")
    if [ ${FAILURE} -ne "2" ]; then
        exit 1
    fi

    # Controller readiness probe
    DELAY=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_READY}.initialDelaySeconds}")
    if [ ${DELAY} -ne "5" ]; then
        exit 1
    fi
    PERIOD=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_READY}.periodSeconds}")
    if [ ${PERIOD} -ne "10" ]; then
        exit 1
    fi
    TIMEOUT=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_READY}.timeoutSeconds}")
    if [ ${TIMEOUT} -ne "9" ]; then
        exit 1
    fi
    SUCCESS=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_READY}.successThreshold}")
    if [ ${SUCCESS} -ne "2" ]; then
        exit 1
    fi
    FAILURE=$(kubectl -n probe-cfg get $pod -o=jsonpath="${JSONPATH_CONTROLLER_READY}.failureThreshold}")
    if [ ${FAILURE} -ne "3" ]; then
        exit 1
    fi
done

kubectl port-forward -n probe-cfg svc/viking-service-probe-cfg ${LOCALPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} $1
