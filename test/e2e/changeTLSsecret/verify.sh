#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../utils.sh

if [ $1 == '1' ]; then
    ORG="Green Midget Cafe"
else
    ORG="Brew HaHa"
fi

LOCALPORT=${LOCALPORT:-4443}

wait_until_ready app=varnish-ingress

wait_until_configured app=varnish-ingress default

kubectl port-forward svc/varnish-ingress ${LOCALPORT}:443 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

CONNECT=cafe.example.com:443:localhost:4443
URI=https://cafe.example.com/coffee/foo/bar
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'HTTP/1.1 200 OK'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E "subject:.+O=${ORG}"
