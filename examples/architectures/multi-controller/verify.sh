#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../../test/utils.sh

COFFEEPORT=${COFFEEPORT:-8888}
TEAPORT=${TEAPORT:-9999}

wait_until_ready app=varnish-ingress-controller kube-system

wait_until_ready app=varnish-ingress cafe
wait_until_configured app=varnish-ingress cafe

kubectl port-forward -n cafe svc/varnish-coffee ${COFFEEPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT

kubectl port-forward -n cafe svc/varnish-tea ${TEAPORT}:80 >/dev/null &

wait_for_port ${COFFEEPORT}
wait_for_port ${TEAPORT}

varnishtest ${TESTOPTS} -Dcoffeeport=${COFFEEPORT} -Dteaport=${TEAPORT} cafe.vtc

# Parse the tea controller log for these lines:
# Ingress class:varnish
# Ingress cafe/tea-ingress configured for Varnish Service cafe/varnish-tea
# Ignoring Ingress cafe/coffee-ingress, Annotation 'kubernetes.io/ingress.class' absent or is not 'varnish'

# Get the name of the tea controller Pod
CTLPOD=$(kubectl get pods -n kube-system -l app=varnish-ingress-controller,example!=coffee -o jsonpath={.items[0].metadata.name})

# Match the logs
kubectl logs -n kube-system $CTLPOD | grep -q 'Ingress class:viking.uplex.de/ingress-controller'
kubectl logs -n kube-system $CTLPOD | grep -q 'Ingress cafe/tea-ingress configured for Varnish Service cafe/varnish-tea'
kubectl logs -n kube-system $CTLPOD | grep -q "Ignoring Ingress cafe/coffee-ingress, IngressClass 'varnish-coffee-class' does not specify 'viking.uplex.de/ingress-controller'"

# Parse the coffee controller log for these lines
# Ingress class:varnish-coffee
# Ingress cafe/coffee-ingress configured for Varnish Service cafe/varnish-coffee
# Ignoring Ingress cafe/tea-ingress, Annotation 'kubernetes.io/ingress.class' absent or is not 'varnish-coffee'

# Get the name of the tea controller Pod
CTLPOD=$(kubectl get pods -n kube-system -l app=varnish-ingress-controller -l example=coffee -o jsonpath={.items[0].metadata.name})

# Match the logs
kubectl logs -n kube-system $CTLPOD | grep -q 'Ingress class:varnish-coffee/ingress-controller'
kubectl logs -n kube-system $CTLPOD | grep -q 'Ingress cafe/coffee-ingress configured for Varnish Service cafe/varnish-coffee'
kubectl logs -n kube-system $CTLPOD | grep -q "Ignoring Ingress cafe/tea-ingress, IngressClass 'viking-controller.uplex.de' does not specify 'varnish-coffee/ingress-controller'"
