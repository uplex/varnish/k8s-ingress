apiVersion: v1
kind: Service
metadata:
  name: pod-template-examples
  labels:
    app: varnish-ingress
    viking.uplex.de/svc: public
spec:
  ports:
  - port: 81
    targetPort: 81
    protocol: TCP
    name: http
  - port: 443
    targetPort: 4443
    protocol: TCP
    name: tls
  selector:
    app: varnish-ingress
    example: env
---
apiVersion: v1
kind: Service
metadata:
  name: pod-template-examples-admin
  labels:
    app: varnish-ingress
  annotations:
    viking.uplex.de/admSecret: adm-secret
spec:
  clusterIP: None
  ports:
  - port: 7000
    targetPort: 7000
    protocol: TCP
    name: varnishadm
  - port: 9000
    targetPort: 9000
    protocol: TCP
    name: configured
  - port: 5555
    targetPort: 5555
    protocol: TCP
    name: dataplane
  - port: 5556
    targetPort: 5556
    protocol: TCP
    name: crt-dnldr
  - port: 9443
    targetPort: 9443
    protocol: TCP
    name: stats
  selector:
    app: varnish-ingress
    example: env
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: env-example
spec:
  replicas: 1
  selector:
    matchLabels:
      app: varnish-ingress
  template:
    metadata:
      labels:
        app: varnish-ingress
        example: env
    spec:
      serviceAccountName: varnish-ingress
      securityContext:
        fsGroup: 998
      containers:
      - image: varnish-ingress/varnish
        imagePullPolicy: IfNotPresent
        name: varnish-ingress
        ports:
        - name: http
          containerPort: 80
        - name: k8s
          containerPort: 8000
        volumeMounts:
        - name: adm-secret
          mountPath: "/var/secret"
          readOnly: true
        - name: varnish-home
          mountPath: "/var/run/varnish-home"
        - name: offload
          mountPath: "/var/run/offload"
        livenessProbe:
          exec:
            command:
            - /usr/bin/pgrep
            - -P
            - "0"
            - varnishd
        readinessProbe:
          httpGet:
            path: /ready
            port: k8s
        args:
          - -n
          - /var/run/varnish-home
        env:
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name

        # Use the PROXY protocol (cf. proxy.yaml).
        - name: PROTO
          value: PROXY

        # Container port for the HTTP listener.
        # MUST match the value set for the http containerPort above, and
        # the http targetPort in the Service.
        - name: HTTP_PORT
          value: "81"

        # Container port for the HTTP readiness check.
        # MUST match the value set for the k8s containerPort above.
        - name: READY_PORT
          value: "8000"

        # Container port for the admin listener.
        # MUST match the value set for the varnishadm containerPort above,
        # and the varnishadm targetPort in the Service.
        - name: ADMIN_PORT
          value: "7000"

        # Container port for the configuration check (returns HTTP
        # status 200 iff an Ingress has been configured)
        # MUST match the targetPort named "configured" in the Service.
        - name: CONFIG_PORT
          value: "9000"

        # Path at which the volume for the admin secret is mounted.
        # MUST match the value of mountPath in volumeMounts above.
        - name: SECRET_PATH
          value: /var/secret

        # Name of the file containing the admin secret.
        # MUST match the value of path in volumes.
        - name: SECRET_FILE
          value: adm.secret

      - image: varnish-ingress/haproxy
        imagePullPolicy: IfNotPresent
        name: varnish-ingress-offloader
        ports:
        - name: tls
          containerPort: 443
        - name: k8s
          containerPort: 8443
        volumeMounts:
        - name: tls-cert
          mountPath: "/etc/ssl/private"
        - name: offload
          mountPath: "/var/run/offload"
        env:
          - name: SECRET_DATAPLANEAPI
            valueFrom:
              secretKeyRef:
                name: adm-secret
                key: dataplaneapi
          - name: POD_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          - name: VARNISH_READY_PORT
            value: "8000"
        livenessProbe:
          exec:
            command:
            - /usr/bin/pgrep
            - -P
            - "0"
            - haproxy
        readinessProbe:
          httpGet:
            path: /healthz
            port: k8s
      volumes:
      - name: adm-secret
        secret:
          secretName: adm-secret
          items:
          - key: admin
            path: adm.secret
      - name: tls-cert
        emptyDir: {}
      - name: varnish-home
        emptyDir:
          medium: "Memory"
      - name: offload
        emptyDir: {}
