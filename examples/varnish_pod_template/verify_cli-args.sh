#! /bin/bash -x

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../test/utils.sh

LOCALPORT=${LOCALPORT:-8888}

set -e
wait_until_ready example=cli-args
wait_until_configured example=cli-args

kubectl port-forward svc/pod-template-examples ${LOCALPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
