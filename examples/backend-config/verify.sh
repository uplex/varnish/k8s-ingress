#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
VTC=$1
source ${MYDIR}/../../test/utils.sh

LOCALPORT=${LOCALPORT:-8888}

wait_until_ready app=varnish-ingress
wait_until_configured app=varnish-ingress

# XXX intermittent 503 responses immediately after configured is true
sleep 5

kubectl port-forward svc/varnish-ingress ${LOCALPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} ${VTC}
