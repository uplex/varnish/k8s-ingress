#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../../test/utils.sh

LOCALPORT=${LOCALPORT:-4443}

wait_until_ready app=varnish-ingress

# Long timeout to wait for the Secret to appear as a certificate on
# the Pods.
wait_until_configured app=varnish-ingress default 600

kubectl port-forward svc/varnish-ingress ${LOCALPORT}:443 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

CONNECT=cafe.example.com:443:localhost:4443
URI=https://cafe.example.com/coffee/foo/bar
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'HTTP/1.1 200 OK'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'Server name: coffee-[a-z0-9]+-[a-z0-9]+$'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'subject:.+CN=cafe.example.com'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'issuer:.+CN=cafe.example.com'

URI=https://cafe.example.com/tea/baz/quux
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'HTTP/1.1 200 OK'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'Server name: tea-[a-z0-9]+-[a-z0-9]+$'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'subject:.+CN=cafe.example.com'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'issuer:.+CN=cafe.example.com'

CONNECT=bar.example.com:443:localhost:4443
URI=https://bar.example.com/whiskey/bar
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'HTTP/1.1 200 OK'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'Server name: whiskey-[a-z0-9]+-[a-z0-9]+$'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'subject:.+CN=bar.example.com'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'issuer:.+CN=bar.example.com'

URI=https://bar.example.com/vodka/martini
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'HTTP/1.1 200 OK'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'Server name: vodka-[a-z0-9]+-[a-z0-9]+$'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'subject:.+CN=bar.example.com'
curl --stderr - -s --connect-to ${CONNECT} -v -k ${URI} | grep -E 'issuer:.+CN=bar.example.com'
