#! /bin/bash -ex

# Nothing special about self-sharding is verified here, just run the
# same tests as for the cafe example ("hello").

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../test/utils.sh

LOCALPORT=${LOCALPORT:-8888}

wait_until_ready app=varnish-ingress
wait_until_configured app=varnish-ingress

# Cached responses from previous tests occasionally interfere with the
# current test, so issue a broad ban first.
PODS=$(kubectl get pod -l app=varnish-ingress -o=name)
for pod in ${PODS}; do
    kubectl exec -t -c varnish ${pod} -- varnishadm -n /run/varnish-home ban obj.status == 200
done

kubectl port-forward svc/varnish-ingress ${LOCALPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

# XXX hackish
# If we run the test too "soon" after the Varnish Services become ready,
# the client may received a 503 response. It doesn't appear to happen if
# we wait a few seconds longer.
# For now, wait longer to run the verification. Should investigate why
# Varnish is not properly configured immediately after self-sharding is
# deployed.
sleep 20
varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
