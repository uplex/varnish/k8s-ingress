#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../test/utils.sh

LOCALPORT=${LOCALPORT:-8888}

wait_until_ready app=varnish-ingress-controller varnish-ingress
wait_until_ready app=varnish-ingress varnish-ingress
wait_until_configured app=varnish-ingress varnish-ingress

kubectl port-forward -n varnish-ingress svc/varnish-ingress ${LOCALPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
